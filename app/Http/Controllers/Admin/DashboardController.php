<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Page;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
   public function index()
   {
   	 return view('admin.index');
   }
   public function allProducts()
   {
   	$products=Product::with('categories')->get();
   	$pcview="product";
   	$category_name='';
   	//echo "<pre>";print_r($products->toArray());die;
   	return view('admin.all-products',compact('products','pcview','category_name'));
   }
   public function allCategories()
   {
   	$categories=Category::all();
   	//echo "<pre>";print_r($categories->toArray());die;
   	return view('admin.all-categories',compact('categories'));
   }
   public function addProduct()
   {
   	$categories=Category::all();
   	return view('admin.add-product',compact('categories'));
   }
   public function deletProduct($id)
   {
   	
   	if(!empty($id))
      {
         Product::where('id',$id)->delete();
         DB::table('category_product')->where('product_id',$id)->delete();
         $message="Product deleted successfully !!!";
      	return redirect('admin/products')->with(['success' => $message]);
      }
   }
   public function addProductPost(Request $request)
   {
   	$this->validate($request,[
                'title'=>'required',
                'price'=>'required',
                'product_image' => 'required|mimes:jpeg,png,jpg|max:2048',
               ]);
   	$Product=new Product();
      $Product->title=$request->title;
      $Product->price=$request->price;
      $Product->description=$request->description;
      $Product->status='1';
      $Product->slug=$this->Wo_SlugPost($request->title);
      if($request->hasFile('product_image'))
     	{
     		$media=$request->product_image;
     		$file_name=$this->Wo_ImportImageFromFile($media, $custom_name = '_product');
         $Product->image=$file_name;
      }
      $Product->quantity=$request->quantity;
      if($Product->save())
      {
      	if(isset($request->category) && count($request->category)>0)
      	{
      		$product_id=$Product->id;
      		foreach ($request->category as $key => $pcat) 
      		{
      			DB::table('category_product')->insert(
					    array('category_id' => $pcat,
					          'product_id' => $product_id)
					);
      		}
      	}

         $message="Product added successfully !!!";
			return redirect()->back()->with(['success' => $message]);
      }
   	//echo "<pre>";print_r($request->all());die;
   }
   public function getCategoryProducts($id)
   {
   	$products=array();
   	$category_name='';
   	if(!empty($id))
   	{
   		$category_products=Category::with('products')->where('id',$id)->first();
	   	//echo "<pre>";print_r($category_products->toArray());die;
	   	if(isset($category_products->products) && count($category_products->products)>0)
	   	{
	   		$products=$category_products->products;
	   	}
	   	$category_name=$category_products->name;
   	}
   	$pcview="category";
   	
   	return view('admin.all-products',compact('products','pcview','category_name'));
   }
   public function getProductDetails($id)
   {
   	$pdetail=Product::with('categories')->where('id',$id)->first();
   	
   	//echo "<pre>";print_r($pdetail->toArray());die;
   	return view('admin.product-details',compact('pdetail'));
   }
   public function getProductEdit($id)
   {
   	$pdetail=Product::with('categories')->where('id',$id)->first();
   	$categories=Category::all();
   	$products_cat=array();
   	if(isset($pdetail->categories) && count($pdetail->categories)>0)
   	{
   		foreach ($pdetail->categories as $key => $pcat) {
   			$products_cat[]=$pcat['id'];
   		}
   	}
   	//echo "<pre>";print_r($products_cat);die;
   	return view('admin.product-edit',compact('pdetail','categories','products_cat'));
   }
   public function productUpdate(Request $request,$id)
   {
   	$this->validate($request,[
                'title'=>'required',
                'price'=>'required'
               ]);
   	 	
        if(!empty($id))
        {
            $Product=Product::find($id);
            $Product->title=$request->title;
            $Product->price=$request->price;
            if($request->hasFile('product_image'))
	        	{
	        		$media=$request->product_image;
	        		$file_name=$this->Wo_ImportImageFromFile($media, $custom_name = '_product');
	            $Product->image=$file_name;
	         }
	         //echo $file_name;die;
            $Product->quantity=$request->quantity;
            if($Product->save())
            {
            	DB::table('category_product')->where('product_id',$id)->delete();
            	if(isset($request->category) && count($request->category)>0)
            	{
            		
            		foreach ($request->category as $key => $pcat) 
            		{
            			DB::table('category_product')->insert(
							    array('category_id' => $pcat,
							          'product_id' => $id)
							);
            		}
            	}

               $message="Product updated successfully !!!";
      			return redirect()->back()->with(['success' => $message]);
            }
        }
   	
   }

   public function addCategory()
   {
   	return view('admin.add-category');
   }
   public function deleteCategory($id)
   {
   	
   	if(!empty($id))
      {
         Category::where('id',$id)->delete();
         DB::table('category_product')->where('category_id',$id)->delete();
         $message="Category deleted successfully !!!";
      	return redirect('admin/categories')->with(['success' => $message]);
      }
   }
   public function addCategoryPost(Request $request)
   {
   	$this->validate($request,[
                'category_name'=>'required|unique:categories,name']
               );
   	$Category=new Category();
   	$Category->name=$request->category_name;
      $Category->slug=$this->Wo_SlugPost($request->category_name);
      
      if($Category->save())
      {
      	$message="Category added successfully !!!";
			return redirect()->back()->with(['success' => $message]);
      }
   	//echo "<pre>";print_r($request->all());die;
   }
   public function getCategoryEdit($id)
   {
   	$category_details=Category::where('id',$id)->first();
   	return view('admin.category-edit',compact('category_details'));
   }
   public function categoryUpdate(Request $request,$id)
   {
   	$this->validate($request,[
                'category_name'=>'required|unique:categories,name,'.$id
               ]);
   	 	
        	if(!empty($id))
        	{
            $Category=Category::find($id);
            $Category->name=$request->category_name;
            $Category->slug=$this->Wo_SlugPost($request->category_name);
            if($Category->save())
            {
            	$message="Category updated successfully !!!";
					return redirect()->back()->with(['success' => $message]);
            }
        }
   		
   }
   public function productDocumentationPages(Request $request)
   {
   	$allpages=Page::OrderBy('id','DESC')->get();
   	return view('admin.all-pages',compact('allpages'));
   }
   public function addNewPage(Request $request)
   {
   	return view('admin.add-page');
   }
   public function addNewPagePost(Request $request)
   {
   	//echo "<pre>";print_r($request->all());die;
   	$this->validate($request,[
                'page_title'=>'required',
                'page_content'=>'required'
             ]
               );
   	$Page=new Page();
   	$Page->title=$request->page_title;
   	$Page->content=$request->page_content;
      $Page->slug=$this->Wo_SlugPost($request->page_title);
      
      if($Page->save())
      {
      	$message="Page added successfully !!!";
			//return redirect()->back()->with(['success' => $message]);
			return  redirect('admin/page/'.$Page->id)->with(['success' => $message]);
      }
   }
   public function getPageEdit($id)
   {
   	$page_details=Page::where('id',$id)->first();
   	return view('admin.edit-page',compact('page_details'));
   }
   public function pageUpdatePost(Request $request,$id)
   {
   	//echo "<pre>";print_r($request->all());die;
   	$this->validate($request,[
                'page_title'=>'required',
                'page_content'=>'required'
             ]
               );
   	if(!empty($id))
     	{
         $Page=Page::find($id);
         $Page->title=$request->page_title;
         $Page->content=$request->page_content;
         if($Page->save())
         {
         	$message="Page updated successfully !!!";
				return redirect()->back()->with(['success' => $message]);
         }
     }
      
   }
   public function deletePage($id)
   {
   	
   	if(!empty($id))
      {
         Page::where('id',$id)->delete();
         $message="Page deleted successfully !!!";
      	return redirect('admin/all-pages')->with(['success' => $message]);
      }
   }
   function Wo_ImportImageFromFile($media, $custom_name = '_url_image',$type = '') 
   {
     	if (empty($media)) {
        return false;
    	}
      if (!file_exists('public/products/images/' . date('Y'))) {
        mkdir('public/products/images/' . date('Y'), 0777, true);
      }
      if (!file_exists('public/products/images/' . date('Y') . '/' . date('m'))) {
        mkdir('public/products/images/' . date('Y') . '/' . date('m'), 0777, true);
      }
    	$extension = 0; //image_type_to_extension($size[2]);
    	if (empty($extension)) {
        $extension = '.jpg';
    	}
    	$dir               = 'public/products/images/' . date('Y') . '/' . date('m');
    	$file_dir          = $dir . '/' . time() . $custom_name . $extension;
    	$fileget           = file_get_contents($media);
    	if (!empty($fileget)) {
        $importImage = @file_put_contents($file_dir, $fileget);
    	}
    	if (file_exists($file_dir)) 
    	{
        $check_image = getimagesize($file_dir);
        if (!$check_image) {
            unlink($file_dir);
        }
        return $file_dir;
    	} 
    	else 
    	{
        return false;
    	}
	}
	function Wo_SlugPost($string) 
	{
    	$slug = $this->url_slug($string, array(
        'delimiter' => '-',
        'limit' => 50,
        'lowercase' => true,
        'replacements' => array(
            '/\b(an)\b/i' => 'a',
            '/\b(example)\b/i' => 'Test'
        )
    	));
    	

    	return $this->checkSlug($slug);
	}
	function checkSlug($slug)
	{
		$pages=Page::where('slug',$slug)->count();
    	if(isset($pages) && $pages>0)
    	{
    		$slug=$slug.'-1';
    		$ifpages=Page::where('slug',$slug)->count();
    		if(isset($ifpages) && $ifpages>0)
    		{
    			$this->checkSlug($slug);
    		}
    	}
		return $slug;
	}
	function url_slug($str, $options = array()) 
	{
	    // Make sure string is in UTF-8 and strip invalid UTF-8 characters
	    $str      = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());
	    $defaults = array(
	        'delimiter' => '-',
	        'limit' => null,
	        'lowercase' => true,
	        'replacements' => array(),
	        'transliterate' => true
	    );
	    // Merge options
	    $options  = array_merge($defaults, $options);
	    $char_map = array(
	        // Latin
	        'À' => 'A',
	        'Á' => 'A',
	        'Â' => 'A',
	        'Ã' => 'A',
	        'Ä' => 'A',
	        'Å' => 'A',
	        'Æ' => 'AE',
	        'Ç' => 'C',
	        'È' => 'E',
	        'É' => 'E',
	        'Ê' => 'E',
	        'Ë' => 'E',
	        'Ì' => 'I',
	        'Í' => 'I',
	        'Î' => 'I',
	        'Ï' => 'I',
	        'Ð' => 'D',
	        'Ñ' => 'N',
	        'Ò' => 'O',
	        'Ó' => 'O',
	        'Ô' => 'O',
	        'Õ' => 'O',
	        'Ö' => 'O',
	        'Ő' => 'O',
	        'Ø' => 'O',
	        'Ù' => 'U',
	        'Ú' => 'U',
	        'Û' => 'U',
	        'Ü' => 'U',
	        'Ű' => 'U',
	        'Ý' => 'Y',
	        'Þ' => 'TH',
	        'ß' => 'ss',
	        'à' => 'a',
	        'á' => 'a',
	        'â' => 'a',
	        'ã' => 'a',
	        'ä' => 'a',
	        'å' => 'a',
	        'æ' => 'ae',
	        'ç' => 'c',
	        'è' => 'e',
	        'é' => 'e',
	        'ê' => 'e',
	        'ë' => 'e',
	        'ì' => 'i',
	        'í' => 'i',
	        'î' => 'i',
	        'ï' => 'i',
	        'ð' => 'd',
	        'ñ' => 'n',
	        'ò' => 'o',
	        'ó' => 'o',
	        'ô' => 'o',
	        'õ' => 'o',
	        'ö' => 'o',
	        'ő' => 'o',
	        'ø' => 'o',
	        'ù' => 'u',
	        'ú' => 'u',
	        'û' => 'u',
	        'ü' => 'u',
	        'ű' => 'u',
	        'ý' => 'y',
	        'þ' => 'th',
	        'ÿ' => 'y',
	        // Latin symbols
	        '©' => '(c)',
	        // Greek
	        'Α' => 'A',
	        'Β' => 'B',
	        'Γ' => 'G',
	        'Δ' => 'D',
	        'Ε' => 'E',
	        'Ζ' => 'Z',
	        'Η' => 'H',
	        'Θ' => '8',
	        'Ι' => 'I',
	        'Κ' => 'K',
	        'Λ' => 'L',
	        'Μ' => 'M',
	        'Ν' => 'N',
	        'Ξ' => '3',
	        'Ο' => 'O',
	        'Π' => 'P',
	        'Ρ' => 'R',
	        'Σ' => 'S',
	        'Τ' => 'T',
	        'Υ' => 'Y',
	        'Φ' => 'F',
	        'Χ' => 'X',
	        'Ψ' => 'PS',
	        'Ω' => 'W',
	        'Ά' => 'A',
	        'Έ' => 'E',
	        'Ί' => 'I',
	        'Ό' => 'O',
	        'Ύ' => 'Y',
	        'Ή' => 'H',
	        'Ώ' => 'W',
	        'Ϊ' => 'I',
	        'Ϋ' => 'Y',
	        'α' => 'a',
	        'β' => 'b',
	        'γ' => 'g',
	        'δ' => 'd',
	        'ε' => 'e',
	        'ζ' => 'z',
	        'η' => 'h',
	        'θ' => '8',
	        'ι' => 'i',
	        'κ' => 'k',
	        'λ' => 'l',
	        'μ' => 'm',
	        'ν' => 'n',
	        'ξ' => '3',
	        'ο' => 'o',
	        'π' => 'p',
	        'ρ' => 'r',
	        'σ' => 's',
	        'τ' => 't',
	        'υ' => 'y',
	        'φ' => 'f',
	        'χ' => 'x',
	        'ψ' => 'ps',
	        'ω' => 'w',
	        'ά' => 'a',
	        'έ' => 'e',
	        'ί' => 'i',
	        'ό' => 'o',
	        'ύ' => 'y',
	        'ή' => 'h',
	        'ώ' => 'w',
	        'ς' => 's',
	        'ϊ' => 'i',
	        'ΰ' => 'y',
	        'ϋ' => 'y',
	        'ΐ' => 'i',
	        // Turkish
	        'Ş' => 'S',
	        'İ' => 'I',
	        'Ç' => 'C',
	        'Ü' => 'U',
	        'Ö' => 'O',
	        'Ğ' => 'G',
	        'ş' => 's',
	        'ı' => 'i',
	        'ç' => 'c',
	        'ü' => 'u',
	        'ö' => 'o',
	        'ğ' => 'g',
	        // Russian
	        'А' => 'A',
	        'Б' => 'B',
	        'В' => 'V',
	        'Г' => 'G',
	        'Д' => 'D',
	        'Е' => 'E',
	        'Ё' => 'Yo',
	        'Ж' => 'Zh',
	        'З' => 'Z',
	        'И' => 'I',
	        'Й' => 'J',
	        'К' => 'K',
	        'Л' => 'L',
	        'М' => 'M',
	        'Н' => 'N',
	        'О' => 'O',
	        'П' => 'P',
	        'Р' => 'R',
	        'С' => 'S',
	        'Т' => 'T',
	        'У' => 'U',
	        'Ф' => 'F',
	        'Х' => 'H',
	        'Ц' => 'C',
	        'Ч' => 'Ch',
	        'Ш' => 'Sh',
	        'Щ' => 'Sh',
	        'Ъ' => '',
	        'Ы' => 'Y',
	        'Ь' => '',
	        'Э' => 'E',
	        'Ю' => 'Yu',
	        'Я' => 'Ya',
	        'а' => 'a',
	        'б' => 'b',
	        'в' => 'v',
	        'г' => 'g',
	        'д' => 'd',
	        'е' => 'e',
	        'ё' => 'yo',
	        'ж' => 'zh',
	        'з' => 'z',
	        'и' => 'i',
	        'й' => 'j',
	        'к' => 'k',
	        'л' => 'l',
	        'м' => 'm',
	        'н' => 'n',
	        'о' => 'o',
	        'п' => 'p',
	        'р' => 'r',
	        'с' => 's',
	        'т' => 't',
	        'у' => 'u',
	        'ф' => 'f',
	        'х' => 'h',
	        'ц' => 'c',
	        'ч' => 'ch',
	        'ш' => 'sh',
	        'щ' => 'sh',
	        'ъ' => '',
	        'ы' => 'y',
	        'ь' => '',
	        'э' => 'e',
	        'ю' => 'yu',
	        'я' => 'ya',
	        // Ukrainian
	        'Є' => 'Ye',
	        'І' => 'I',
	        'Ї' => 'Yi',
	        'Ґ' => 'G',
	        'є' => 'ye',
	        'і' => 'i',
	        'ї' => 'yi',
	        'ґ' => 'g',
	        // Czech
	        'Č' => 'C',
	        'Ď' => 'D',
	        'Ě' => 'E',
	        'Ň' => 'N',
	        'Ř' => 'R',
	        'Š' => 'S',
	        'Ť' => 'T',
	        'Ů' => 'U',
	        'Ž' => 'Z',
	        'č' => 'c',
	        'ď' => 'd',
	        'ě' => 'e',
	        'ň' => 'n',
	        'ř' => 'r',
	        'š' => 's',
	        'ť' => 't',
	        'ů' => 'u',
	        'ž' => 'z',
	        // Polish
	        'Ą' => 'A',
	        'Ć' => 'C',
	        'Ę' => 'e',
	        'Ł' => 'L',
	        'Ń' => 'N',
	        'Ó' => 'o',
	        'Ś' => 'S',
	        'Ź' => 'Z',
	        'Ż' => 'Z',
	        'ą' => 'a',
	        'ć' => 'c',
	        'ę' => 'e',
	        'ł' => 'l',
	        'ń' => 'n',
	        'ó' => 'o',
	        'ś' => 's',
	        'ź' => 'z',
	        'ż' => 'z',
	        // Latvian
	        'Ā' => 'A',
	        'Č' => 'C',
	        'Ē' => 'E',
	        'Ģ' => 'G',
	        'Ī' => 'i',
	        'Ķ' => 'k',
	        'Ļ' => 'L',
	        'Ņ' => 'N',
	        'Š' => 'S',
	        'Ū' => 'u',
	        'Ž' => 'Z',
	        'ā' => 'a',
	        'č' => 'c',
	        'ē' => 'e',
	        'ģ' => 'g',
	        'ī' => 'i',
	        'ķ' => 'k',
	        'ļ' => 'l',
	        'ņ' => 'n',
	        'š' => 's',
	        'ū' => 'u',
	        'ž' => 'z'
	    );
	    // Make custom replacements
	    $str      = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
	    // Transliterate characters to ASCII
	    if ($options['transliterate']) {
	        $str = str_replace(array_keys($char_map), $char_map, $str);
	    }
	    // Replace non-alphanumeric characters with our delimiter
	    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
	    // Remove duplicate delimiters
	    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
	    // Truncate slug to max. characters
	    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
	    // Remove delimiter from ends
	    $str = trim($str, $options['delimiter']);
	    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
	}
}
