<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Page;
use Illuminate\Support\Facades\DB;
use Session;
use App\Country;
use Illuminate\Support\Facades\Auth;
use App\UserShipping;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.index');
    }
    public function getSearchProducts(Request $request)
    {
        $products=array();
        $search=$request->q;
        if(isset($search) && $search !='')
        {
            $products=Product::where('title','like',"%".$search."%")->paginate(24);
            
        }
        return view('frontend.search-result',compact('products','search'));
    }
    public function getCollections()
    {
        $collections =array();
        //$categories = Category::with('onlyOneProduct')->get();
        $collections = Category::orderBy('name','DESC')->get();
        $collections->each(function($category) {
            $category->load('onlyProduct');
        });
        //echo "<pre>";print_r($collections->toArray());die;
        return view('frontend.collections',compact('collections'));
    }
    public function getCollectionProducts(Request $request,$slug)
    {
        $collectionProducts=array();
        if(!empty($slug))
        {
            $category = Category::where('slug',$slug)->first();
            $order='ASC';
            if(isset($request->sort_by) && $request->sort_by =='title-ascending')
            {
                $order='ASC';
                $products = $category->products()->orderBy('price',$order)->paginate(24);
            }
            else if(isset($request->sort_by) && $request->sort_by =='title-descending')
            {
                $order='DESC';
                $products = $category->products()->orderBy('price',$order)->paginate(24);
            }
            else if(isset($request->sort_by) && $request->sort_by =='price-descending')
            {
                $price='DESC';
                $products = $category->products()->orderBy('price',$price)->paginate(24);
            }
            else if(isset($request->sort_by) && $request->sort_by =='price-ascending')
            {
                $price='ASC';
                $products = $category->products()->orderBy('price',$price)->paginate(24);
                //echo "<pre>";print_r($products->toArray());die;
            }
            else if(isset($request->sort_by) && $request->sort_by =='created-ascending')
            {
                $created='ASC';
                $products = $category->products()->orderBy('created_at',$created)->paginate(24);
            }
            else if(isset($request->sort_by) && $request->sort_by =='created-descending')
            {
                $created='DESC';
                $products = $category->products()->orderBy('created_at',$created)->paginate(24);
            }
            else
            {
                $products = $category->products()->orderBy('price',$order)->paginate(24);
            }
            
            
        }
        $view_by='grid';
        if(isset($request->view) && $request->view =='list')
        {
            $view_by='list';
        }
        //echo "<pre>";print_r($collectionProducts->toArray());die;
        return view('frontend.product-listing',compact('products','category','slug','view_by'));
    }
    public function getProductDetail($slug)
    {
        $productdetail=array();
        if(!empty($slug))
        {
            $productdetail=Product::with('categories')->where('slug',$slug)->first();
        }
        //echo "<pre>";print_r($productdetail->toArray());die;
        return view('frontend.product-detail',compact('productdetail'));
    }
    public function getPagesDetail($slug)
    {
        $pagedetail=array();
        if(!empty($slug))
        {
            $pagedetail=Page::where('slug',$slug)->first();
        }
        //echo "<pre>";print_r($productdetail->toArray());die;
        return view('pages.page-detail',compact('pagedetail'));
    }
   public function addToCart(Request $request)
   {
      $cart = session()->get('cart');
      $result=["status"=>0,'message'=>'Input not found!!'];
      if(isset($request->productid) && $request->productid !='')
      {
        $id=$request->productid;
        $product=Product::where('id',$id)->first();
        if(isset($request->quantity) && $request->quantity >0)
        {
          if(!empty($product))
          {
            $cart['product'][$id]= $product->toArray();
            $cart['product'][$id]['qty']= $request->quantity;
            if(isset($cart['total_price']))
            {
                 $cart['total_price'] += $cart['product'][$id]['price'] * $request->quantity;
            }
            else
            {
                 $cart['total_price'] = $cart['product'][$id]['price'] * $request->quantity;
            }
          }
          
        }
        elseif(isset($cart['product'][$id]))
        {
          unset($cart['product'][$id]);
        }
        session()->put('cart', $cart); 
         $result=["status"=>1,'result'=>$cart,'message'=>'Your product has been added into the cart.'];
      }
      return $result;
   }
   public function updateCart(Request $request)
   {
      $cart = session()->get('cart');
      $result=["status"=>0,'message'=>'Input not found!!'];
      if(isset($request->productid) && $request->productid !='')
      {
        $id=$request->productid;
        $product=Product::where('id',$id)->first();
        if(isset($request->quantity) && $request->quantity >0)
        {
          if(!empty($product))
          {
            $cart['product'][$id]= $product->toArray();
            $cart['product'][$id]['qty']= $request->quantity;
            
          }
          
        }
        elseif(isset($cart['product'][$id]))
        {
          unset($cart['product'][$id]);
        }
        session()->put('cart', $cart); 
         $result=["status"=>1,'result'=>$cart,'message'=>'Your product has been added into the cart.'];
      }
      return $result;
   }
  public function removeFromCart(Request $request)
  {

    if(isset($request->productid) && $request->productid !='') 
    {
      $cart = session()->get('cart');

      if(isset($cart['product'][$request->productid])) 
      {
        $productdetails=$cart['product'][$request->productid];
        $product_price=$productdetails['price']*$productdetails['qty'];
        $cart['total_price'] = $cart['total_price']-$product_price;
        unset($cart['product'][$request->productid]);
        session()->put('cart', $cart);
      }
      session()->flash('success', 'Product removed successfully');
      $cart = session()->get('cart');
      if(isset($cart['product']) && count($cart['product']) <=0)
      {
        $cart['total_price'] =0;
        session()->put('cart', $cart);
      }
      $result=["status"=>1,'result'=>$cart,'message'=>'Product removed successfully'];
      return  $result;

    }
  }

  public function myShoppingCart()
  {
    $cartitems = session()->get('cart');
    //echo "<pre>";print_r($cart);die;
    return view('pages.my-cart',compact('cartitems'));
  }
  public function cartCheckout()
  {
    $cartitems = session()->get('cart');
    $countries=Country::all();
    //echo "<pre>";print_r($cart);die;
    $shippings=array();
    if(Auth::check())
    {
      $user_id=Auth::user()->id;
      $shippings=UserShipping::where('user_id',$user_id)->first();
    }
    //echo "<pre>";print_r($shippings);die;
    if(isset($cartitems['product']) && count($cartitems['product'])>0)
    {
        return view('pages.checkout',compact('cartitems','countries','shippings'));
    }
    else
    {
        return redirect()->route('my_cart');
    }
    
  }

}
