<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Contact;
class PagesController extends Controller
{
   public function news()
   {
   	 return view('pages.news');
   }
   public function contactUs()
   {
   	 return view('pages.contact-us');
   }
   public function contactPost(Request $request)
   {
     $request->validate([
          'phone_number' => 'required|numeric',
          'email_address'=>'required|email'
        ]);

        $contact = new Contact;
        $contact->company=$request->company_name;
        $contact->first_name=$request->first_name;
        $contact->last_name=$request->last_name;
        $contact->email=$request->email_address;
        $contact->phone=$request->phone_number;
        $contact->comments=$request->comments;


       
        if($contact->save())
        {
            return back()->with('success', 'Thanks for contacting us!! We will contact you shortly.');
        }

        
   }
   public function termsAndConditions()
   {
   	 return view('pages.terms-conditions');
   }
   public function faq()
   {
   	 return view('pages.faq');
   }
   
   public function aboutUs()
   {
       return view('pages.about-us');
   }
   public function productDocumentation()
   {
      $collection=Page::OrderBy('title','ASC')->get();
      $results = $collection->sortBy('title')->groupBy(function ($item, $key) {
        return substr($item['title'], 0, 1);
      });
      
      // foreach ($results as $key => $result) {
      
      //  foreach ($result as $key => $page) {
      //    echo "<pre>";print_r($page->title);die;
      //  }
      // }
       return view('pages.product-documentation',compact('results'));
   }
}
