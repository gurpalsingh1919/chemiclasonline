<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\MatchOldPassword;
use App\Country;
use App\User;
use Auth, Hash;
class DashboardController extends Controller
{
   public function myAccount()
   {
   	$countries=Country::all();
   	return view('user.my-account',compact('countries'));
   }
   public function updateUserPost(Request $request)
   {
   	//echo "<pre>";print_r($request->all());die;
   	$user_id=Auth::user()->id;
   	$this->validate($request,[
                'first_name'=>'required',
                'last_name'=>'required',
                'company' => 'required',
                'address1' => 'required',
                'address2' => 'required',
                'city' => 'required',
                'country' => 'required',
                'zip_code' => 'required',
                'phone' => 'required',
               ]);
   	
   	$user=User::find($user_id);
   	$user->first_name=$request->first_name;
   	$user->last_name=$request->last_name;
   	$user->company=$request->company;
   	$user->address1=$request->address1;
   	$user->address2=$request->address2;
   	$user->city=$request->city;
   	$user->country=$request->country;
   	$user->zip_code=$request->zip_code;
   	$user->phone=$request->phone;
   	if($user->save())
   	{
   		$message="You have successfully updated your account details !!!";
      	return redirect()->back()->with(['success' => $message]);
   	}

   }
   public function changePassword()
   {
   	return view('user.change-password');
   }
   public function changePasswordPost(Request $request)
   {
   	 $request->validate([
          'current_password' => 'required',
          'password' => 'required|string|min:6|confirmed',
          'password_confirmation' => 'required',
        ]);

        $user = Auth::user();

        if (!Hash::check($request->current_password, $user->password)) {
            return back()->with('error', 'Current password does not match!');
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return back()->with('success', 'Password successfully changed!');

   }
  /* public function myOrders()
   {
   	$countries=Country::all();
   	return view('user.my-account',compact('countries'));
   }*/

}
