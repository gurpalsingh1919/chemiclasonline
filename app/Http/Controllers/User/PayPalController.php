<?php
  
namespace App\Http\Controllers\User;
  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\User;
use App\Order; 
use App\OrderDetail; 
use App\Product;
use App\Category;
use App\Page;
use App\UserShipping;
use App\Country;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use Response;  
use Log;
use DateTime;
class PayPalController extends Controller
{
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
        
    public function makepayment(Request $request)
    {
    	//echo "<pre>";print_r(session()->get('cart'));die;
        if(Auth::check())
        {
            $user_id=Auth::user()->id;

            $data = [];
            $cart = session()->get('cart');
            //echo "<pre>";print_r($cart);//print_r($cart['review_section_1']);
            
            
            if(isset($cart['product']) && count($cart['product'])>0)
            {
                $this->validate($request, [
                'first_name' => 'required',
                'last_name'=>'required',
                'address' => 'required',
                'apartment' => 'required',
                'phone' => 'required',
                'city'=> 'required',
                'country'=> 'required',
                'state'=> 'required',
                'zipcode'=> 'required'
                ]);
                $usershippings=array();
                $usershippings['first_name']=$request->first_name;
                $usershippings['last_name']=$request->last_name;
                $usershippings['address']=$request->address;
                $usershippings['apartment']=$request->apartment;
                $usershippings['phone']=$request->phone;
                $usershippings['city']=$request->city;
                $usershippings['country']=$request->country;
                $usershippings['state']=$request->state;
                $usershippings['zipcode']=$request->zipcode;
                $usershippings['user_id']=$user_id;
                $shippings=UserShipping::where('user_id',$user_id)->first();
                if(!empty($shippings))
                {
                    $shippings->update($usershippings);
                }
                else
                {
                    UserShipping::create($usershippings);
                }
                $Order=new Order;
                $Order['amount']=0;
                $Order['user_id']=$user_id;
                $Order->save();

                $data=$this->addOrderDetails($Order->id);
                

                $user_order=Order::find($Order->id);
                $user_order->amount =number_format($data['total_price'],2);
                $user_order->save();
                $data['invoice_id'] = $user_order->id;
            }
            else
            {
                $message="Please add at least one item";
                return redirect()->route('collections')
                    ->with('error',$message);
            }
           //echo "<pre>";print_r($data);
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('user.payment.success');
            $data['cancel_url'] = route('user.payment.cancel');
            $data['total'] = number_format($data['total_price'],2);
            $request->session()->put('order_id', $data['invoice_id']);
            $provider = new ExpressCheckout;
      
            $response = $provider->setExpressCheckout($data);
      
            //$response = $provider->setExpressCheckout($data, true);
            //echo "<pre>";print_r($response);die;
            return redirect($response['paypal_link']);
        }
        else
        {
        		return redirect()->route('login');
        }
    }
   
    public function addOrderDetails($order_id)
    {
        $data = [];
        
        $total_price=0;
        $cart = session()->get('cart');
        $user_id=Auth::user()->id;
        if(isset($cart['product']))
        {   $products=array();
            foreach ($cart['product'] as $key => $value) 
            {
                $arr_item=array();
                $arr_item['name']=$value['title'];
                $arr_item['price']=$value['price'];
                $arr_item['qty']=$value['qty'];
                $data['items'][] =$arr_item;
                $total_price +=$value['price']*$value['qty'];
                
                 array_push($products, $value['id']);

                $OrderDetail= new OrderDetail;
                $OrderDetail['product_id']=$value['id'];
                $OrderDetail['order_id']=$order_id;
                $OrderDetail['qty']=$value['qty'];
                $OrderDetail['price']=$value['price'];
                $OrderDetail['subtotal']=$value['price']*$value['qty'];
                $OrderDetail->save();
            }

        }
        
        $data['total_price']=$total_price;
        return $data;
        
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Ht*tp\Response
     */
    public function cancel(Request $request)
    {
        //dd('Your payment is canceled. You can create cancel page here.');
        $order_id = session()->get('order_id');
        if(!empty($order_id))
        {
            $userOrder = Order::find($order_id);
            if(!empty($request->query('token')))
            {
                $userOrder->payment_id=$request->query('token');
            }
            $userOrder->status='3';
            $userOrder->save();
            $request->session()->forget('cart');
            return redirect('/user/transactions/'.$order_id)->withError('Payment is canceled !!!');
        }
        else
        {
            $request->session()->forget('cart');
            return redirect('/cart')->withError('Payment is canceled !!!');
        }
        

    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
      //echo "<pre>";print_r($request->all());die;
        $order_id = session()->get('order_id');
        if (empty($request->query('PayerID')) || empty($request->query('token')))
        {
            $userOrder = Order::find($order_id);
            $userOrder->status='2';
            $userOrder->save();
            return redirect('/user/transactions/'.$order_id)->withError('Payment was not successful. Please Try Again !!');
        }

        $provider = new ExpressCheckout;
        $response = $provider->getExpressCheckoutDetails($request->token);

        //echo "<pre>";print_r($response);
        	if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) 
        	{
            if(isset($order_id))
            {
               $transactions = Order::where('id',$order_id)
                    ->with('orderdetail')
                    ->first();
                   
               $transactions->payment_id=$request->query('token');
               $transactions->payer_id=$request->query('PayerID');
               $transactions->status='1';
               $transactions->save();
            }
         	$request->session()->forget('cart');
         	return redirect('user/transactions/'.$order_id)->withSuccess('Payment made successfully.');
        }
        else
        {
            $userOrder = Order::find($order_id);
            $userOrder->status='2';
            $userOrder->save();
            return redirect('/user/transactions/'.$order_id)->withError('Payment authentication problem. Please Try Again !!');
        }
  
       // dd('Something is wrong.');
    }
    public function getTransactionid($provider,$transaction)
    {
        //echo "<pre>";print_r($transaction->subscriptiondetail[0]['plan']);die;
        $data=array();
        $arr_item=array();
        if($transaction->plan_type=='2')
        {
            if(isset($transaction->subscriptiondetail[0]['plan']) && $transaction->subscriptiondetail[0]['plan'] =='1')
            {
                $arr_item['name']='Monthly Unlimited';
                $arr_item['price']='24.99';
            }
            else if(isset($transaction->subscriptiondetail[0]['plan']) && $transaction->subscriptiondetail[0]['plan'] =='2')
            {
                $arr_item['name']='Yearly Unlimited';
                $arr_item['price']='199.00';
            }
            
        }
        
        $arr_item['qty']='1';
        $data['items'][] =$arr_item;
        $data['invoice_id'] = $transaction->id;
        $data['invoice_description'] = "Order #{$transaction->id} Invoice";
        $data['total'] = number_format($transaction->amount,2);
        $responses = $provider->doExpressCheckoutPayment($data, $_GET['token'], $_GET['PayerID']);
        //echo "<pre>";print_r($responses);die;
        return $responses['PAYMENTINFO_0_TRANSACTIONID'];
    }
    public function transactionHistory()
    {
        if(Auth::check())
        {
            $user_id=Auth::user()->id;
            $ordertransactions = Order::where('user_id',$user_id)
             ->with('orderdetail')
             ->orderBy('id','DESC')
             ->get();
            //echo "<pre>";print_r($ordertransactions->toArray());die;
            foreach ($ordertransactions as $key => $transactions) 
            {
                if(isset($transactions->orderdetail))
                {
                  	$transactions['test_detail']='';
                   // echo "<pre>";print_r($transactions->orderdetail);die;
                  	foreach ($transactions->orderdetail as $key => $value) 
                  	{
                     	$productdetails=Product::where('id',$value->product_id)->first();
              			$arr=$productdetails->toArray();
                        if(isset($transactions->orderdetail[$key]->product))
                        {
                            $transactions->orderdetail[$key]->product[]=$arr;
                        }
                        else
                        {
                            $transactions->orderdetail[$key]->product=array();
                            //$transactions->orderdetail[$key]->product_name=$arr['title'];
                            $transactions->orderdetail[$key]->product=$arr;
                        }
              			
                   } 
                }
            }
        //echo "<pre>";print_r($ordertransactions->toArray());die;
        return view('user.transactions',compact('ordertransactions'));
      }
    }
   public function transactionDetails($order_id)
   {
     if(Auth::check())
     {
         $user_id=Auth::user()->id;
         $transactions = Order::where('user_id',$user_id)
            ->where('id',$order_id)
            ->with('orderdetail','user')
            ->first();
         if(isset($transactions->orderdetail) && count($transactions->orderdetail) >0)
         {   
            $transactions['product']=array();
            $product_detail=array();
            foreach ($transactions->orderdetail as $key => $value) 
            {
               $productdetails=Product::where('id',$value->product_id)->first();
             	$arr=$productdetails->toArray();
             	$product_detail[]=$arr;
            }
            $transactions['product']=$product_detail;
         } 
      }
      //echo "<pre>";print_r($transactions->toArray());die;
      return view('user.order-detail',compact('transactions'));
   }

  
    /**
 * Retrieve IPN Response From PayPal
 *
 * @param \Illuminate\Http\Request $request
 */
    public function realpayment_ipn(Request $request)
    {
        Log::channel('single')->info(json_encode($request->all()));
        Storage::disk('local')->put('transactions-'.$request->invoice_id.'.txt', json_encode($request->all()));
        $provider = new ExpressCheckout;
        
        $request->merge(['cmd' => '_notify-validate']);
        $post = $request->all();        
        
        $response = (string) $provider->verifyIPN($post);
        
        if ($response === 'VERIFIED' && isset($request->invoice_id)) 
        {   
            if($request->payment_status=='Completed')
            {
                $order_id=$request->invoice_id;              
                $transactions = userOrder::where('id',$order_id)->with('orderdetail')->first();
                if(isset($transactions) && $transactions->status !='1')
                {
                    $transactions->payment_id=$request->txn_id;
                    $transactions->payer_id=$request->payer_id;
                    $transactions->status='1';
                    $transactions->save();
                    if(isset($transactions->orderdetail) && count($transactions->orderdetail) >0)
                    {
                        $new_mocktest=json_decode($transactions->orderdetail[0]['type_id']);
                        //print_r($new_mocktest);
                        $user_detail=User::find($transactions->user_id);
                        $old_mocktest=json_decode($user_detail->mocktest);
                        

                        $newone=array_merge($new_mocktest,$old_mocktest);
                        //print_r(json_encode(array_unique($newone)));
                        $user_detail->mocktest=json_encode(array_unique($newone));
                        $user_detail->save();
                        //print_r(json_encode(array_unique($newone)));die;
                    }
                }
            }
            
            
        } 
        else
        {
            Log::channel('single')->info("Some thing went wrong in the payment !");
            //Log::info("Some thing went wrong in the payment !");
        }                           
    }  
    public function refundMypayment(Request $request)
    {
        //echo "<pre>";print_r($request->all());die;
        $trans_id=$request->trans_id;
        $price=0;
        $transactionid='';
        $subscription=Subscription::where(['id'=>$trans_id,'status'=>'1'])->first();
        if(isset($subscription) && $subscription->count()>0)
        {
            $plan=$subscription->plan;
            $start_date=$subscription->start_date;
            $eligible_time= date('Y-m-d', strtotime($start_date. ' + 5 days'));


            $order_id=$subscription->order_id;
            $UserOrder=UserOrder::find($order_id);
            $transactionid=$UserOrder->transaction_id;

            if($plan=='1')
            {   //echo $eligible_time.'--'.$this->monthly;die; 
                if($eligible_time>=date('Y-m-d'))
                {
                    $price=$this->monthly;
                }
            }
            else if($plan=='2')
            {
                if($eligible_time>=date('Y-m-d'))
                {
                    $price=$this->yearly;
                }
                else
                {
                    $d1 = new DateTime($start_date);
                    $d2 = new DateTime();
                    //$interval = date_diff($date1, $date2);
                    $months= $d1->diff($d2)->m;
                    $days  = $d1->diff($d2)->d;
                    var_dump($d1->diff($d2)->m);
                    var_dump($d1->diff($d2)->d);

                    if($days >=5)
                    {
                        $months=$months+1;
                    }
                    $price = $this->monthly*$months;

                }
            }
            
        }
        //echo $price .'----'.$transactionid;die;
        if($price>0)
        {
            $provider = new ExpressCheckout;
            //$response = $provider->refundTransaction($transactionid);
            // To issue partial refund, you must provide the amount as well for refund:
            $response = $provider->refundTransaction($transactionid, $price); 

            //echo "<pre>";print_r($response['L_LONGMESSAGE0']);die;
            if(isset($response['ACK']) && $response['ACK']=='Success')
            {
                $order_id=$subscription->order_id;
                $subscription->status='4';
                $subscription->save();

                $userOrder = userOrder::find($order_id);
                $userOrder->status='4';
                $userOrder->refund_amount=$price;
                $userOrder->refund_response=json_encode($response);
                $userOrder->save();
                $message="<b>Refund successfully done. !!</b>";
                return redirect()->back()->with(['success' => $message]);
            }
            else
            {
                //$message="<b>Something went wrong. Please try again after sometime!!</b>";
                return redirect()->back()->with(['success' => $response['L_LONGMESSAGE0']]);
            }
            //echo "<pre>";print_r($response);die;
        }
        else
        {
            $message="<b>You are not eligible for refund !!</b>";
            return redirect()->back()->with(['success' => $message]);
            
        }      
    }
}
