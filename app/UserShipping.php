<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShipping extends Model
{
    protected $fillable = [
        'user_id','first_name','last_name','address','apartment','city','country','zipcode','phone','state'
    ];
}
