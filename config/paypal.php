<?php
/**
 * PayPal Setting & API Credentials
 * Created by Raza Mehdi <srmk@outlook.com>.
 */

return [
    'mode'    => env('PAYPAL_MODE', 'sandbox'), // Can only be 'sandbox' Or 'live'. If empty or invalid, 'live' will be used.
    'sandbox' => [
        'username'    => env('PAYPAL_SANDBOX_API_USERNAME', 'sso.phhc_api1.gmail.com'),
        'password'    => env('PAYPAL_SANDBOX_API_PASSWORD', 'DCPKEBD4AQQAPHED'),
        'secret'      => env('PAYPAL_SANDBOX_API_SECRET', 'AgZz2Dh23JOLR1A969Btn79aj2ZhAW9c6CAfAwaDp6Fhi0XWTWdSvxB1'),
        'certificate' => env('PAYPAL_SANDBOX_API_CERTIFICATE', ''),
        'app_id'      => 'APP-80W284485P519543T', // Used for testing Adaptive Payments API in sandbox mode
    ],
    'live' => [
        'username'    => env('PAYPAL_LIVE_API_USERNAME', 'realprepaws_api1.gmail.com'),
        'password'    => env('PAYPAL_LIVE_API_PASSWORD', 'DDW4NKAEQFSX947R'),
        'secret' => env('PAYPAL_LIVE_API_SECRET', 'AOEHDKgzgcxHF.6pBdwUhcC5uomcAzF.yQqVn.51RtQwnCWuNfYxFmjN'),
        'certificate' => env('PAYPAL_LIVE_API_CERTIFICATE', ''),
        'app_id'      => '', // Used for Adaptive Payments API
    ],

    'payment_action' => 'Sale', // Can only be 'Sale', 'Authorization' or 'Order'
    'currency'       => env('PAYPAL_CURRENCY', 'USD'),
    'billing_type'   => 'MerchantInitiatedBilling',
    'notify_url'     => '', // Change this accordingly for your application.
    'locale'         => 'en_US', // force gateway language  i.e. it_IT, es_ES, en_US ... (for express checkout only)
    'validate_ssl'   => true, // Validate SSL when creating api client.
];
