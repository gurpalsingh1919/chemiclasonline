$(function() {

    // Basic editors
    // ------------------------------

    // Default initialization
    $('.summernote').summernote();


    // Control editor height
    $('.summernote-height').summernote({
        height: 400
    });


    // Click to edit
    // ------------------------------

    // Edit
    $('#edit').on('click', function() {
        $('.click2edit').summernote({focus: true});
    })

    // Save
    $('#save').on('click', function() {
        var aHTML = $('.click2edit').summernote('code'); //save HTML If you need(aHTML: array).
        console.log(aHTML);
        var content=$(".summernote").summernote("code");
         $('#page_content').val(content);
        $('.click2edit').summernote('destroy');
    })
    $('#update').on('click', function() {
        var content=$(".summernote").summernote("code");
        $('#page_details').val(content);
    })

    var page_details=$('#page_details').val();
    var content=$(".summernote").summernote("code",page_details);


});
