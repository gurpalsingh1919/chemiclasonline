@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
  <div class="min-height-200px">
    <div class="page-header">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="title">
            <h4>Products</h4>
          </div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Products</a></li>
              <li class="breadcrumb-item active" aria-current="page">Add Product</li>
            </ol>
          </nav>
        </div>
        <div class="col-md-6 col-sm-12 text-right">
          <a class="btn btn-primary" href="{{route('admin.products')}}" role="button">Back
          </a>
        </div>
      </div>
    </div>

    <!-- Select-2 Start -->
    <div class="pd-20 card-box mb-30">
      <div class="clearfix">
        <div class="pull-left">
          <h4 class="text-blue h4">Add New Product</h4>
          
        </div>
      </div>
      @if(session('error')) 
           <div class="alert alert-danger mb-4" role="alert"> <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('error') !!}</div>
            
           @endif
           @if(session('success')) 
           <div class="alert alert-success mb-4" role="alert"> 
            <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('success') !!}</div>
            
         @endif
      <form class="row" action="{{route('admin.add_product_post')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-6">
          <label>*Product Title</label>
          <input class="form-control" name="title" value="{{old('title')}}" type="text" placeholder="Product title">
          @if ($errors->has('title'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('title') }}</strong>
            </span>
          @endif 
        </div>
        <div class="form-group col-md-6">
          <label>*Price ($)</label>
          <input class="form-control" name="price" value="{{old('price')}}" type="text" placeholder="Product price in $">
          @if ($errors->has('price'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('price') }}</strong>
            </span>
          @endif 
                  
        </div>
        <div class="form-group col-md-6">
          <label>Quantity</label>
          <input class="form-control" name="quantity" value="{{old('quantity')}}" type="text" placeholder="Product quantity">
        </div>
        <div class="form-group col-md-6">
          <label>Description</label>
          <textarea name="description" placeholder="Product description" class="form-control">{{old('description')}}</textarea>
        </div>
        
        <div class="form-group col-md-6">
          <label>*Image</label>
          <input name="product_image" type="file" class="form-control-file form-control height-auto">
           @if ($errors->has('product_image'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('product_image') }}</strong>
            </span>
          @endif 
        </div>
        <div class="form-group">
          <h4 class="text-blue h4">Select Categories</h4>
          <div class="row select_category">
            
            <!-- <div class="col-md-6 col-sm-12"> -->
              
              @foreach($categories as $category)
              <div class="col-md-6 custom-control custom-checkbox mb-5">
                <input type="checkbox" name="category[]" class="custom-control-input" value="{{$category->id}}" id="customCheck{{$category->id}}-1">
                <label class="custom-control-label" for="customCheck{{$category->id}}-1">{{$category->name}}</label>
              </div>
              @endforeach
            <!-- </div> -->
            
          </div>
        </div>
        
        
        <div class="form-group col-md-4">
          
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </form>
    </div>
    

  </div>
  
</div>
<style type="text/css">
  .select_category .custom-control 
  {
    padding-left: 63px;   
  }
</style>
@endsection('content')