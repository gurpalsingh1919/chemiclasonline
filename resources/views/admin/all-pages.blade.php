@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
	<div class="min-height-200px">
		<div class="page-header">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="title">
						<h4>Pages</h4>
					</div>
					<nav aria-label="breadcrumb" role="navigation">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Pages</li>
						</ol>
					</nav>
				</div>
				<div class="col-md-6 col-sm-12 text-right">
		         <a class="btn btn-primary" href="{{route('admin.add_new_page')}}" role="button">
		              Create Page
		         </a>
		      </div>
			</div>
		</div>
		<!-- Export Datatable start -->
		<div class="card-box mb-30">
			<div class="pd-20">
				<h4 class="text-blue h4">All Pages</h4>
			</div>
			<div class="pb-20">
				<table class="data-table table stripe hover nowrap" data-page-length='50'>
					@if(count($allpages)>0)
					<thead>
						<tr>
							<th>#</th>
							<th>Page Name</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($allpages as $page)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$page->title}}</td>
							<td>@if($page->status=='1')
								{{__('Active')}}
								@else
								{{__('Pending')}}
								@endif
							</td>
							<td>
								<div class="dropdown">
                           <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                               <i class="dw dw-more"></i>
                           </a>
                           <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                               <a class="dropdown-item" href="{{route('pages_details',$page->slug)}}" target="_blank"><i class="dw dw-eye"></i> View</a>
                               <a class="dropdown-item" href="{{route('admin.page_edit',$page->id)}}"><i class="dw dw-edit2"></i> Edit</a>
                               <a class="dropdown-item" href="{{route('admin.page_delete',$page->id)}}"  onclick="return confirm('Are you sure to want delete this?')"><i class="dw dw-delete-3"></i> Delete</a>
                           </div>
                       </div>
							</td>
						</tr>
						@endforeach
					</tbody>
					@endif
				</table>
			</div>
		</div>
		<!-- Export Datatable End -->
	</div>
</div>
@endsection('content')