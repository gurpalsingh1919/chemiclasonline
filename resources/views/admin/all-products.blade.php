@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
	<div class="min-height-200px">
		<div class="page-header">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="title">
						<h4>Products</h4>
					</div>
					@if($pcview=='category')
					<nav aria-label="breadcrumb" role="navigation">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{route('admin.categories')}}">Categories</a></li>
							<li class="breadcrumb-item active" aria-current="page">{{$category_name}}</li>
						</ol>
					</nav>


					@else
					<nav aria-label="breadcrumb" role="navigation">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Products</li>
						</ol>
					</nav>
					@endif
				</div>
				<div class="col-md-6 col-sm-12 text-right">
		         <a class="btn btn-primary" href="{{route('admin.add_product')}}" role="button">
		              Create Product
		         </a>
		      </div>
			</div>
		</div>
		<!-- Export Datatable start -->
		<div class="card-box mb-30">
			<div class="pd-20">
				<h4 class="text-blue h4">{{$category_name}} All Products</h4>
			</div>
			<div class="pb-20">
				<table class="table hover multiple-select-row data-table-export nowrap">
					@if(count($products)>0)
					<thead>
						<tr>
							<th>#</th>
							<th class="table-plus datatable-nosort">Product</th>
							<th>Name</th>
							<th>Price</th>
							<th width="20%">Categories</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($products as $product)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td class="table-plus">
                        <img src="{{ asset('')}}{{$product->image}}" width="70" height="70" alt="">
                     </td>
							<td>{{$product->title}}</td>
							<td>${{number_format($product->price,2)}}</td>
							<td>
								@if(isset($product->categories) && count($product->categories)>0)
								<i class="icon-copy fa fa-tag" aria-hidden="true"></i>
								@foreach($product->categories as $cat)
								<span class="badge badge-success" 
								style="font-size: 8px;">{{$cat->name}}</span>
									@if($loop->iteration %2==0)
									 <br/>
									@endif 
								@endforeach
								@endif
							</td>
							<td>@if($product->status=='1')
								{{__('Active')}}
								@else
								{{__('Pending')}}
								@endif
							</td>
							<td>
								<!-- <a href="{{route('admin.product_details',$product->id)}}" class="btn btn-primary">View</a> -->
								<div class="dropdown">
                           <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                               <i class="dw dw-more"></i>
                           </a>
                           <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                               <a class="dropdown-item" href="{{route('admin.product_details',$product->id)}}"><i class="dw dw-eye"></i> View</a>
                               <a class="dropdown-item" href="{{route('admin.product_edit',$product->id)}}"><i class="dw dw-edit2"></i> Edit</a>
                               <a class="dropdown-item" href="{{route('admin.product_delete',$product->id)}}"  onclick="return confirm('Are you sure to want delete this?')"><i class="dw dw-delete-3"></i> Delete</a>
                           </div>
                       </div>
							</td>
						</tr>
						@endforeach
					</tbody>
					@endif
				</table>
			</div>
		</div>
		<!-- Export Datatable End -->
	</div>
</div>
@endsection('content')