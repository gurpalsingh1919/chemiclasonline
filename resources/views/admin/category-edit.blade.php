@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
  <div class="min-height-200px mb-30">
    <div class="page-header">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="title">
            <h4>Category</h4>
          </div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{route('admin.categories')}}">Category</a></li>
              <li class="breadcrumb-item active" aria-current="page">Edit Category</li>
            </ol>
          </nav>
        </div>
        <div class="col-md-6 col-sm-12 text-right">
          <a class="btn btn-primary" href="{{route('admin.categories')}}" role="button">Back
          </a>
        </div>
      </div>
    </div>

    <!-- Select-2 Start -->
    <div class="pd-20 card-box mb-30">
      <div class="clearfix">
        <div class="pull-left">
          <h4 class="text-blue h4">Edit Category</h4>
          
        </div>
      </div>
      @if(session('error')) 
           <div class="alert alert-danger mb-4" role="alert"> <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('error') !!}</div>
            
           @endif
           @if(session('success')) 
           <div class="alert alert-success mb-4" role="alert"> 
            <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('success') !!}</div>
            
         @endif
      <form class="row" action="{{route('admin.category_update',$category_details->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-12">
          <label>*Category Name</label>
          <input class="form-control" name="category_name" value="{{$category_details->name}}" type="text" placeholder="category name">
          @if ($errors->has('category_name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('category_name') }}</strong>
            </span>
          @endif 
        </div>
        <div class="form-group col-md-4">
          <button type="submit" class="btn btn-primary btn-block">Update</button>
        </div>
      </form>
    </div>
    

  </div>
  
</div>
<style type="text/css">
  .select_category .custom-control 
  {
    padding-left: 63px;   
  }
</style>
@endsection('content')