@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
  <div class="min-height-200px">
    <div class="page-header">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="title">
            <h4>Pages</h4>
          </div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{route('admin.all_pages')}}">Pages</a></li>
              <li class="breadcrumb-item active" aria-current="page">Update Page</li>
            </ol>
          </nav>
        </div>
        <div class="col-md-6 col-sm-12 text-right">
          <a class="btn btn-primary" href="{{route('pages_details',$page_details->slug)}}" role="button" target="_blank">View
          </a>
        </div>
      </div>
    </div>

    <!-- Select-2 Start -->
    <div class="pd-20 card-box mb-30">
      <div class="clearfix">
        <div class="pull-left">
          <h4 class="text-blue h4">Update Page</h4>
          
        </div>
      </div>
      @if(session('error')) 
           <div class="alert alert-danger mb-4" role="alert"> <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('error') !!}</div>
            
           @endif
           @if(session('success')) 
           <div class="alert alert-success mb-4" role="alert"> 
            <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('success') !!}</div>
            
         @endif
      <form class="row" action="{{route('admin.page_update',$page_details->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-6">
          <label>*Page Title</label>
          <input class="form-control" name="page_title" value="{{$page_details->title}}" type="text" placeholder="Page title">
          @if ($errors->has('page_title'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('page_title') }}</strong>
            </span>
          @endif 
        </div>
        <div class="form-group col-md-12">
          <label>*Page Content</label>
          <input type="hidden" name="page_content" id="page_details" value="{{$page_details->content}}">
          <div class="summernote"></div>
          @if ($errors->has('page_content'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('page_content') }}</strong>
            </span>
          @endif 
                  
        </div>
                
        <div class="form-group col-md-4">
          
          <button id="update" type="submit" class="btn btn-primary btn-block">Update</button>
        </div>
      </form>
    </div>
    

  </div>
  
</div>
 <style type="text/css">
  .flex_container_pd {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
.rte {
    margin-bottom: 15px;
}
.flex_container_pd > .flex_item_left_1_pd {
    width: 33%;
}
.flex_container_pd > .flex_item_right_2_pd {
    width: 67%;
    padding-left: 2%;
}
.rte table {
    table-layout: fixed;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    min-width: 50%;
}
th, td {
    text-align: left;
    padding: 15px;
    border: 1px solid #dedede;
}
.flex_container_pd_supporting {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
.flex_container_pd_supporting > .flex_item_pd_supporting {
    text-align: center;
    width: 20%;
}

a {
    color: #9c756f;
    text-decoration: none;
    background: transparent;
}
img.auto, .grid-item img, .grid-item iframe {
    max-width: 100%;
}
</style>
@endsection('content')