<div class="left-side-bar">
		<div class="brand-logo">
			<a href="{{route('admin.dashboard')}}">
				<img src="{{ asset('public/admin/vendors/images/logo.png') }}" alt="" class="dark-logo">
				<img src="{{ asset('public/admin/vendors/images/logo-white.png') }}" alt="" class="light-logo">
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					
					<li>
						<a href="{{route('admin.dashboard')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-house-1"></span>
							<span class="mtext">Dashboard</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-trees"></span><span class="mtext">Products</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.products')}}">All Products</a></li>
							<li><a href="{{route('admin.add_product')}}">Add New</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-tag"></span><span class="mtext">Categories</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.categories')}}">All Categories</a></li>
							<li><a href="{{route('admin.add_category')}}">Add New</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-copy"></span>
							<span class="mtext">Product Documentation</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.all_pages')}}">All Pages</a></li>
							<li><a href="{{route('admin.add_new_page')}}">Add New Page</a></li>
						</ul>
					</li>
					
					
				</ul>
			</div>
		</div>
	</div>