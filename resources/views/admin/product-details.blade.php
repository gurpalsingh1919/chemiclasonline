@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
	<div class="min-height-200px">
		<div class="page-header">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="title">
						<h4>Product Detail</h4>
					</div>
					<nav aria-label="breadcrumb" role="navigation">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{route('admin.products')}}">Products</a></li>
							<li class="breadcrumb-item active" aria-current="page">Product Detail</li>
						</ol>
					</nav>
				</div>
				<div class="col-md-6 col-sm-12 text-right">
		         <a class="btn btn-primary" href="{{route('admin.products')}}" role="button">
		              Back
		         </a>
		      </div>
			</div>
		</div>
		<div class="product-wrap">
			<div class="product-detail-wrap mb-30">
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="product-slider slider-arrow">
							<div class="product-slide">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
							<div class="product-slide">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
							<div class="product-slide">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
							<div class="product-slide">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
						</div>
						<!-- <div class="product-slider-nav">
							<div class="product-slide-nav">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
							<div class="product-slide-nav">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
							<div class="product-slide-nav">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
							<div class="product-slide-nav">
								<img src="{{asset('')}}{{$pdetail->image}}" alt="">
							</div>
						</div> -->
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="product-detail-desc pd-20 card-box height-100-p">
							<h4 class="mb-20 pt-20">{{$pdetail->title}}</h4>
							<p>{{$pdetail->description}}
								@if($pdetail->description =='')
									Description
								@endif
							<div class="price">
								<del>${{number_format($pdetail->price,2)}}</del><ins>${{number_format($pdetail->price,2)}}</ins>
							</div>
							<div class="mx-w-150">
								<div class="form-group">
									<label class="text-blue">quantity</label>
									<input id="demo3_22" type="text" 
									value="{{$pdetail->quantity}}" name="demo3_22">
								</div>
							</div>
							<div class="categories" >
								<i class="icon-copy fa fa-tag" aria-hidden="true"></i>
								<label class="text-blue">Categories</label><br/>
							@if(isset($pdetail->categories) && count($pdetail->categories)>0)
								
								@foreach($pdetail->categories as $cat)
								<span class="badge badge-success" style="font-size: 8px">{{$cat->name}}</span><br/>
									
								@endforeach
								@endif
							</div><br/><br/>
							<div class="row">
								<div class="col-md-6 col-6">
									<a href="{{route('admin.product_edit',$pdetail->id)}}" class="btn btn-primary btn-block">Edit</a>
								</div>
								<div class="col-md-6 col-6">
									<button class="btn btn-outline-primary btn-block">
									In Stock
								</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <h4 class="mb-20">Recent Product</h4>
			<div class="product-list">
				<ul class="row">
					<li class="col-lg-4 col-md-6 col-sm-12">
						<div class="product-box">
							<div class="producct-img"><img src="vendors/images/product-img1.jpg" alt=""></div>
							<div class="product-caption">
								<h4><a href="#">Gufram Bounce Black</a></h4>
								<div class="price">
									<del>$55.5</del><ins>$49.5</ins>
								</div>
								<a href="#" class="btn btn-outline-primary">Read More</a>
							</div>
						</div>
					</li>
					<li class="col-lg-4 col-md-6 col-sm-12">
						<div class="product-box">
							<div class="producct-img"><img src="vendors/images/product-img2.jpg" alt=""></div>
							<div class="product-caption">
								<h4><a href="#">Gufram Bounce White</a></h4>
								<div class="price">
									<del>$75.5</del><ins>$50</ins>
								</div>
								<a href="#" class="btn btn-outline-primary">Add To Cart</a>
							</div>
						</div>
					</li>
					<li class="col-lg-4 col-md-6 col-sm-12">
						<div class="product-box">
							<div class="producct-img"><img src="vendors/images/product-img3.jpg" alt=""></div>
							<div class="product-caption">
								<h4><a href="#">Contrast Lace-Up Sneakers</a></h4>
								<div class="price">
									<ins>$80</ins>
								</div>
								<a href="#" class="btn btn-outline-primary">Add To Cart</a>
							</div>
						</div>
					</li>
				</ul>
			</div> -->
		</div>
	</div>
</div>
@endsection('content')