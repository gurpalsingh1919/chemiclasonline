@extends('layouts.admin-app')

@section('content')
<div class="xs-pd-20-10">
	<div class="min-height-200px">
		<div class="page-header">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="title">
						<h4>Advanced Components</h4>
					</div>
					<nav aria-label="breadcrumb" role="navigation">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{route('admin.products')}}">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Product Edit</li>
						</ol>
					</nav>
				</div>
				<div class="col-md-6 col-sm-12 text-right">
		          <a class="btn btn-primary" href="{{route('admin.products')}}" role="button">
		              Back
		            </a>
		      </div>
			</div>
		</div>

		<!-- Select-2 Start -->
		<div class="pd-20 card-box mb-30">
			<div class="clearfix">
				<div class="pull-left">
					<h4 class="text-blue h4">Product Update</h4>
					
				</div>
			</div>
			@if(session('error')) 
           <div class="alert alert-danger mb-4" role="alert"> <i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('error') !!}</div>
            
           @endif
           @if(session('success')) 
           <div class="alert alert-success mb-4" role="alert"> 
           	<i class="flaticon-cancel-12 close" data-dismiss="alert"></i>{!! session('success') !!}</div>
            
         @endif
			<form class="row" action="{{route('admin.product_update',$pdetail->id)}}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="form-group col-md-6">
					<label>*Product Title</label>
					<input class="form-control" name="title" value="{{$pdetail->title}}" type="text" placeholder="Product title">
				</div>
				<div class="form-group col-md-6">
					<label>*Price ($)</label>
					<input class="form-control" name="price" value="{{$pdetail->price}}" type="text">
				</div>
				<div class="form-group col-md-6">
					<label>Quantity</label>
					<input class="form-control" name="quantity" value="{{$pdetail->quantity}}" type="text">
				</div>
				<div class="form-group col-md-6">
					<label>Description</label>
					<textarea name="description" class="form-control">{{$pdetail->description}}</textarea>
				</div>
				
				<div class="form-group col-md-6">
					<label>*Image</label>
					<input name="product_image" type="file" class="form-control-file form-control height-auto">
					@if(!empty($pdetail->image))
					<img src="{{asset('')}}{{$pdetail->image}}" alt="" height="70px" width="70px">
					@endif
				</div>
				<div class="form-group">
					<h4 class="text-blue h4">Select Categories</h4>
					<div class="row select_category">
						
						<!-- <div class="col-md-6 col-sm-12"> -->
							
							@foreach($categories as $category)
							<div class="col-md-6 custom-control custom-checkbox mb-5">
								<input type="checkbox" name="category[]" class="custom-control-input" value="{{$category->id}}" id="customCheck{{$category->id}}-1" {{(in_array($category->id,$products_cat)?'checked':'')}}>
								<label class="custom-control-label" for="customCheck{{$category->id}}-1">{{$category->name}}</label>
							</div>
							@endforeach
						<!-- </div> -->
						
					</div>
				</div>
				
				
				<div class="form-group col-md-4">
					
					<button type="submit" class="btn btn-primary btn-block">Update</button>
				</div>
			</form>
		</div>
		

	</div>
	
</div>
<style type="text/css">
	.select_category .custom-control 
	{
    padding-left: 63px;   
	}
</style>
@endsection('content')