@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb">
          <a href="{{route('home')}}">Home</a> <span>></span> Collection</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection productListSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
           @if(isset($collections) && count($collections) >0)
          @foreach($collections as $collection)
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="product"> 
              <a href="{{route('collection_products',$collection->slug) }}">
                <div class="imageThumb">
                  <img src="{{asset($collection->onlyProduct[0]->image)}}" alt="" class="imgResponsive" >
                </div>
                <div class="productName">{{$collection->name}}</div>
              </a> 
            </div>
          </div>
          @endforeach
          @endif
      
        </div>
      </div>
    </div>
  </div>
</section>
@endsection('content')