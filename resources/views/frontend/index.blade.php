@extends('layouts.frontend-app')

@section('content')
<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="banner"> <img src="{{ asset('public/frontend/images/banner.jpg') }}" alt="" class="imgResponsive"> </div>
      </div>
    </div>
    <div class="categoriesBanner">
      <div class="row">
        <div class="col-lg-4 col-md-6"> <a href ="{{route('collection_products','beverage')}}">
            <img src="{{ asset('public/frontend/images/distilled-spirits.jpg') }}" alt="" class="imgResponsive" ></a> 
        </div>
        <div class="col-lg-4 col-md-6"> <a href ="{{route('collection_products','herbal-extraction')}}">
            <img src="{{ asset('public/frontend/images/extraction.jpg') }}" alt="" class="imgResponsive" ></a> </div>
        <div class="col-lg-4 col-md-6"> <a href ="{{route('collection_products','food-flavor-fragrance')}}">
            <img src="{{ asset('public/frontend/images/food-flavor.jpg') }}" alt="" class="imgResponsive" ></a> </div>
        <div class="col-lg-4 col-md-6"> <a href ="{{route('collection_products','industrial')}}">
            <img src="{{ asset('public/frontend/images/industrial.jpg') }}" alt="" class="imgResponsive" ></a> </div>
        <div class="col-lg-4 col-md-6"> <a href ="{{route('collection_products','small-molecule-pharma')}}">
            <img src="{{ asset('public/frontend/images/life-science.jpg') }}" alt="" class="imgResponsive" ></a> </div>
        <div class="col-lg-4 col-md-6"> <a href ="{{route('collection_products','personal-care-cosmetics')}}">
          <img src="{{ asset('public/frontend/images/personal-care.jpg') }}" alt="" class="imgResponsive" ></a> </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="text-center">
          <h2>FOR MORE INFORMATION ABOUT WHO WE ARE AND WHAT WE DO, <a class="click_here_main" href="https://greenfield.com/pharmco/" rel="nofollow" target="_blank">PLEASE CLICK HERE!</a> </h2>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection('content')