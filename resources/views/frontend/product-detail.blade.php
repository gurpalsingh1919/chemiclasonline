@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> 
          @if(isset($productdetail->categories) && count($productdetail->categories)>0)
            @if(isset($productdetail->categories[0]))
            <a href="{{route('collection_products',$productdetail->categories[0]->slug)}}">{{$productdetail->categories[0]->name}}</a> 
            @endif
          @endif
          <span>></span> {{(isset($productdetail->title))?$productdetail->title:''}}</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="success-message"></div>
      <div class="col-md-5">
    <div class="imageLargeThumb">
      <img src="{{asset($productdetail->image)}}" alt="" class="imgResponsive" ></div>
      </div>
      <div class="col-md-7">
        <h2>{{(isset($productdetail->title))?$productdetail->title:''}}</h2>
          <div class="productPrice productPriceLarge">${{$productdetail->price}}<sup>00</sup></div>
        <label>Quantity</label>
        <div class="quantity buttons_added">
          <input type="button" value="-" class="minus">
          <input type="number" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
          <input type="button" value="+" class="plus">
        </div>
        <a href="#" onclick="addtoCart('{{$productdetail->id}}')" class="customBtn01 transparentBtn">
          <i class="fa fa-shopping-cart"></i> Add to Cart</a> <br>
        <a href="#" class="customBtn01">Buy it Now</a> 
      </div>
    </div>
  </div>
</section>

@endsection('content')