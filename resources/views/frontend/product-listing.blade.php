@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb">
          <a href="{{route('home')}}">Home</a> <span>></span>{{(isset($category->name))?$category->name:''}}</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection productListSection">
  <div class="container">
    <div class="row">
      <div class="col-md-3 pr-0">
        <h3>Shop By</h3>
        <ul class="subCategories">
          @php
            $randomcollections=getRandomCollections();
          @endphp
          @foreach($randomcollections as $collection)
            <li>
              <a href="{{route('collection_products',$collection->slug)}}" title="{{$collection->name}}">{{$collection->name}}</a>
            </li>
          @endforeach
          
        </ul>
      </div>
      <div class="col-md-9 border-left">
        
        <div class="row">
          <div class="col-lg-6 order-2 order-lg-1">
          <h1>{{(isset($category->name))?$category->name:''}}</h1>
          </div>
          <div class="col-lg-6 order-1 order-lg-12 text-right">
            <div class="filterBox">
              
              <div class="dropdownMenu">
                <form action="{{ route('collection_products',$slug)}}">
              Sort by<select onchange="this.form.submit()" name="sort_by">
                  <!-- <option value="manual">Featured</option>
                  <option value="best-selling">Best selling</option> -->
                  <option value="title-ascending">Alphabetically, A-Z</option>
                  <option value="title-descending">Alphabetically, Z-A</option>
                  <option value="price-ascending">Price, low to high</option>
                  <option value="price-descending">Price, high to low</option>
                  <option value="created-ascending">Date, old to new</option>
                  <option value="created-descending">Date, new to old</option>
                </select>
              </form>
              </div>
              
              <div class="viewIcons">
                <a href="?view=grid"><i class="fa fa-th-large" aria-hidden="true"></i></a> 
                <a href="?view=list"><i class="fa fa-th-list" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          @if(isset($products) && count($products) >0)

            @foreach($products as $product)
              @if($view_by=='list')
                <div class="col-md-12">
                  <div class="product text-center"> 
                    <a href="{{route('product_details',$product->slug)}}">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="imageThumb">
                          <img src="{{ asset($product->image) }}" alt="" class="imgResponsive" ></div>
                      </div>
                      <div class="col-md-7">
                        <div class="productName">{{$product->title}}</div>
                      </div>
                      <div class="col-md-2">
                        <div class="productPrice">${{$product->price}}<sup>00</sup></div>
                      </div>
                    </div>
                    </a> 
                  </div>
                </div>
              @else
              <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="product"> 
                  <a href="{{route('product_details',$product->slug)}}">
                  <div class="imageThumb">
                    <img src="{{ asset($product->image) }}" alt="" class="imgResponsive" ></div>
                  <div class="productName">{{$product->title}}</div>
                  <div class="productPrice">${{$product->price}}<sup>00</sup></div>
                  </a> 
                </div>
              </div>
              @endif
            @endforeach
          @endif
        </div>
      </div>
    </div>
    
    <div class="row paginationOuter">
        <div class="col-md-12">
          {{  $products->links()  }}
        </div>
    </div>
  </div>
</section>
<style type="text/css">
  .paginationOuter .pagination
  {
    justify-content: center !important;
  }
</style>
@endsection('content')