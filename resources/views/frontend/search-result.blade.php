@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb">
          <a href="{{route('home')}}">Home</a> <span>></span>{{(isset($search))?$search:''}}</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection productListSection">
  <div class="container">
    <div class="row">
      
      <div class="col-md-12">
        
        <div class="row">
          @if(isset($products) && count($products) >0)

            @foreach($products as $product)
             
              <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="product"> 
                  <a href="{{route('product_details',$product->slug)}}">
                  <div class="imageThumb">
                    <img src="{{ asset($product->image) }}" alt="" class="imgResponsive" ></div>
                  <div class="productName">{{$product->title}}</div>
                  <div class="productPrice">${{$product->price}}<sup>00</sup></div>
                  </a> 
                </div>
              </div>
              
            @endforeach
          @else
            <div class="col-md-12">
            <div class="mt-5 mb-5 text-center">
              <div class="emptyCartIcon mt-2 mb-3">
                <img src="{{asset('public/frontend/images/no-search-results.png')}}" />
              </div>
              <h1 class="display-4 mb-3">Sorry, no results found!</h1>
              <p class="lead  mb-4"><strong>Please check the spelling or try searching for something else :)</p>
              <hr>
              <p class="lead">
                <a href="{{route('pages.contact_us')}}" class="ml-1 mr-1 customBtn01 transparentBtn">NEED HELP?</a> 
                <a href="{{route('collections')}}" class="ml-1 mr-1 customBtn01">Continue Shopping</a>
              </p>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
    @if(count($products)>0)
    <div class="row paginationOuter">
        <div class="col-md-12">
          {{  $products->links()  }}
        </div>
    </div>
    @endif
  </div>
</section>
<style type="text/css">
  .paginationOuter .pagination
  {
    justify-content: center !important;
  }
</style>
@endsection('content')