<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/admin/vendors/images/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/admin/vendors/images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/admin/vendors/images/favicon-16x16.png') }}">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@700&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/frontend/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('public/frontend/css/core.css') }}">
<link rel="stylesheet" href="{{ asset('public/frontend/css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('public/frontend/css/font-awesome.min.css') }}">
</head>
<style type="text/css">
  .invalid-feedback {
     display: block;
    }
</style>
<body>
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-4"> <a class="navbar-brand logo" href="{{route('home')}}">
        <img src="{{ asset('public/frontend/images/logo.png') }}" alt="" class="imgResponsive" /></a></div>
      <div class="col-md-8 text-right">
        <div class="loginButtons">
            @guest
                <a href="{{route('login')}}">Sign in </a> or <a href="{{route('register')}}">Create an Account </a>
            @else
                <a href="{{route('user.my_account')}}">My Account </a> |
                <a href="{{ route('logout') }}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest
        </div>
        <div class="searchBar">
          <form action="{{route('product_search')}}">
            <div class="input-group">
              <input type="text" name="q" class="form-control boxShadRad searchBarInput" placeholder="Search all products..." />
              <div class="input-group-btn">
                <button class="btn btn-default searchButton boxShadRad" type="submit"> <i class="fa fa-search"></i> </button>
              </div>
            </div>
          </form>
          @php
            $cartitems = session()->get('cart');
          @endphp
          <a href="{{ route('my_cart') }}" class="header-cart-btn cart-toggle">
          <i class="fa fa-shopping-cart"></i>
          Cart <span class="cart-count badge badge-light cart-badge--desktop">{{(isset($cartitems['product']) && count($cartitems['product'])>0)?count($cartitems['product']):'0'}}</span>
        </a>
        </div>
      </div>
    </div>
  </div>
</header>
<nav class="navbar navbar-expand-lg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> <span class="navbar-toggler-icon"></span> <span class="navbar-toggler-icon"></span> </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav">
            <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop By Products</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <ul class="subMenu">
                  <li><a class="dropdown-item" href="{{route('collection_products','organic-alcohols')}}"> ORGANIC ALCOHOLS </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','non-gmo-alcohols')}}"> NON-GMO ALCOHOLS </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','pure-ethanol')}}">PURE ETHANOL </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','tax-free-ethanol')}}">TAX FREE ETHANOL </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','denatured-alcohol')}}">DENATURED ALCOHOL </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','ipa-isopropyl-alcohol')}}">IPA (ISOPROPYL ALCOHOL) </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','acetone')}}">ACETONE </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','water')}}"> WATER </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','ethyl-acetate')}}"> ETHYL ACETATE </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','n-heptane')}}">N-HEPTANE </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','acetonitrile')}}">ACETONITRILE </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','mct-oil')}}">MCT OIL </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','glycerin')}}">GLYCERIN </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','organic-alcohols')}}">SEE ALL PRODUCTS </a> </li>
                </ul>
              </div>
            </li>
            <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop By Industry</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <ul class="subMenu">
                  <li><a class="dropdown-item" href="{{route('collection_products','small-molecule-pharma')}}"> SMALL MOLECULE PHARMA </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','large-molecule-pharma-bioprocessing')}}"> LARGE MOLECULE PHARMA/BIOPROCESSING </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','labs')}}"> LABS </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','beverage')}}"> BEVERAGE </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','industrial')}}"> INDUSTRIAL </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','personal-care-cosmetics')}}"> PERSONAL CARE/COSMETICS </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','herbal-extraction')}}"> HERBAL EXTRACTION </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','medical-device')}}"> MEDICAL DEVICE </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','nutraceutical')}}"> NUTRACEUTICAL </a> </li>
                  <li><a class="dropdown-item" href="{{route('collection_products','food-flavor-fragrance')}}"> FOOD/FLAVOR/FRAGRANCE </a> </li>
                </ul>
              </div>
            </li>
            <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Support</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <ul class="subMenu">
                  <li><a class="dropdown-item" href="{{route('pages.product_documentation')}}"> Product Documentation </a> </li>
                  <li><a class="dropdown-item" href="{{route('pages.faq')}}"> FAQ&#39;S </a> </li>
                  <li><a class="dropdown-item" href="{{route('pages.terms_conditions')}}"> Terms &amp; Conditions </a> </li>
                  <li><a class="dropdown-item" href="{{route('pages.contact_us')}}"> Contact Us </a> </li>
                </ul>
              </div>
            </li>
            <li class="nav-item"> 
              <a class="nav-link" href="{{route('pages.news')}}">News</a> 
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>
@yield('content')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>SITEMAP</h3>
        <ul class="footerNavigation">
          <li><a href="#">Home</a></li>
          <li><a href="#">Greenfield Global</a></li>
          <li><a href="{{ route('user.my_account') }}">My Account</a></li>
          <li><a href="{{ route('my_cart') }}">Shopping Cart</a></li>
          <li><a href="#">Have a Question?</a></li>
          <li><a href="{{route('pages.faq')}}">FAQ'S</a></li>
        </ul>
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <p>© Copyright 2021 chemicalsonline. All rights reserved.
        <p> 
      </div>
      <div class="col-md-6">
        <ul class="footerNavigation text-right">
          <li><a href="{{route('pages.about_us')}}">About Us</a></li>
          <li><a href="{{route('pages.contact_us')}}">Contact Us</a></li>
          <li><a href="#">Help</a></li>
          <li><a href="#">Legal Notice</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<script src="{{ asset('public/frontend/js/jquery.min.js') }}"></script> 
<script src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('public/frontend/js/core.js') }}"></script>

<script type="text/javascript">
  $('.plus').on('click',function(e){

    var val = parseInt($(this).prev('input').val());

    $(this).prev('input').val( val+1 );


    });
    $('.minus').on('click',function(e){

    var val = parseInt($(this).next('input').val());
    if(val !== 0){
        $(this).next('input').val( val-1 );
    } });

  function addtoCart(product_id) 
  {
    //alert(product_id)
    var cart_url="{{ route('my_cart') }}";
    var productid=product_id;
    var qty=$('.qty').val()
    var csrf_token = $('meta[name=csrf-token]').attr('content');
    console.log(productid +'--'+qty)
    $.ajax({
      url: 'add-to-cart',
      type: 'POST',
      data: {_token: csrf_token, productid:product_id,quantity:qty},
      success: function (data) { 
        var msg='<div class="alert alert-success mb-4" role="alert">'+data.message+' </div>'
        $("#success-message").html(msg);
        
        setTimeout(function () { window.location.href=cart_url; }, 2000);
                      
      }
    });

  }
  function removeProduct(product_id) 
  {
    //alert(product_id)
    var productid=product_id;
    var csrf_token = $('meta[name=csrf-token]').attr('content');
    $.ajax({
      url: 'remove-from-cart',
      type: 'POST',
      data: {_token: csrf_token, productid:product_id},
      success: function (data) { 
        console.log(data)
        var msg='<div class="alert alert-success mb-4" role="alert">'+data.message+' </div>'
        $("#success-message").html(msg);
        setTimeout(function () { location.reload(1); }, 5000);
                      
      }
    });

  }
</script>


</body>
</html>
