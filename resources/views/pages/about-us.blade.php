@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> ABOUT US</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>ABOUT US</h1>


            <h3>Greenfield is the largest high-purity alcohol company in North America offering bulk and packaged ingredients and additives. Under the Pharmco brand, we offer a wide range of specialty alcohols and bio-based chemicals to quality-driven and environmentally-conscious customers around the globe. The Pharmco brand has come to stand for premium quality and service excellence. We have been supplying life science, food, flavor, fragrance, personal care, extraction and industrial companies with mission-critical products for more than 30 years.</h3>

            <h3>Businesses ranging from start-ups to the largest companies in the world trust Pharmco with their specialty chemical and ingredient needs because Pharmco provides:</h3>
            <ul class="listing01">
            <li>A level of quality and service that small suppliers can’t replicate and large suppliers can’t react to. We are not simply brokers. We control the process from order receipt to manufacturing to delivery.</li>
            <li>A personal touch. We know our customers and provide extraordinary access to our internal team to make sure your needs are met.</li>
            <li>Certifications and standards including GMP, Organic, Non-GMO Project Verified, and ISO 9001:2015. Our products are tested and certified to meet requirements of United States Pharmacopeia, European Pharmacopeia, Japanese Pharmacopeia, American Chemical Society and Food Chemical Codex monographs, as well as other federal and custom specifications..</li>
            <li>Lab capabilities ranging from simple wet chemistry to sophisticated metals testing on our inductively coupled plasma mass spectrometry (ICP-MS) instruments.</li>
            <li>Twelve federally registered Distilled Spirits Plant (DSP) locations offering regional distribution from coast to coast and through more than 40 international distribution partners.</li>
            <li>An unbroken chain of custody for ethanol supplied from our parent producer, Greenfield Global.</li>
            <li>Sterile and bio-friendly solvents and bioprocess solutions.</li>
            <li>Custom ethanol cuts and the widest range of alcohol formulas and solvent blends.</li>
            <li>Product scale-up from kilos to metric tons, with package sizes ranging from pints to railcars.</li>
            <li>A commitment to reducing our global reliance on fossil fuels and petroleum-based products.</li>
            </ul>


      </div>
    </div>
  </div>
</section>
@endsection('content')