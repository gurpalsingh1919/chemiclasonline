@extends('layouts.frontend-app')

@section('content')
<section class="contentSection shippingPageContent">
  <div class="container">
    <div class="row">
      <div class="col-md-6 order-md-1 order-12">
        <form action="{{route('user.make_payment')}}" method="post">
          @csrf
        <div>
          <h2>Shipping Address</h2>
          
            <div class="row">
              <div class="col-lg-6">
                <label for="firstname">First Name</label>
                <input type="text" id="text" name="first_name" value="{{(isset($shippings->first_name) && $shippings->first_name !='')? $shippings->first_name:old('first_name')}}">
                @error('first_name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-lg-6">
                <label for="lastname">Last Name</label>
                <input type="text" id="text" name="last_name" value="{{(isset($shippings->last_name) && $shippings->last_name !='')? $shippings->last_name:old('last_name')}}">
                @error('last_name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-md-12">
                <label for="Email">Address</label>
                <input type="text" id="text" name="address" value="{{(isset($shippings->address) && $shippings->address !='')? $shippings->address:old('address')}}">
                @error('address')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-md-6">
                <label for="apt">Apartment, Suite, etc.</label>
                <input type="text" id="apt" name="apartment" value="{{(isset($shippings->apartment) && $shippings->apartment !='')? $shippings->apartment:old('apartment')}}">
                @error('apartment')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-md-6">
                <label for="phone">Phone</label>
                <input type="text" id="phone" name="phone" value="{{(isset($shippings->phone) && $shippings->phone !='')? $shippings->phone:old('phone')}}">
                @error('phone')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-md-12">
                <label for="city">City</label>
                <input type="text" id="city" name="city" value="{{(isset($shippings->city) && $shippings->city !='')? $shippings->city:old('city')}}">
                @error('city')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-lg-4">
                <label for="country">Country</label>
                <select name="country" id="country" >
                @foreach($countries as $country)
                  <option value="{{$country->id}}" {{((isset($shippings->country) && $shippings->country==$country->id) || old('county')==$country->id)?"selected":''}}>{{$country->name}}</option>
                @endforeach
              </select>
                @error('country')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-lg-4">
                <label for="state">State</label>
                <input type="text" id="state" name="state" value="{{(isset($shippings->state) && $shippings->state !='')? $shippings->state:old('state')}}">
                @error('state')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
              <div class="col-lg-4">
                <label for="zipcode">Zipcode</label>
                <input type="text" id="zipcode" name="zipcode" value="{{(isset($shippings->zipcode) && $shippings->zipcode !='')? $shippings->zipcode:old('zipcode')}}">
                @error('zipcode')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
               @enderror
              </div>
            </div>
         
        </div>
        <div class="cartFinalInfo">
          <div>
            <a href="{{route('my_cart')}}" class="ml-1 mr-1 customBtn01 transparentBtn">Return to Cart</a> 
            <a href="{{route('collections')}}" class="ml-1 mr-1 customBtn01">Continue Shopping</a></div>
        </div>
      </div>
      <div class="col-md-6 order-md-12 order-1">
        <h2>Shopping Cart</h2>
        <div class="cartBlock">
          @if(isset($cartitems['product']) && count($cartitems['product'])>0)
          <table class="table table-striped cart w-100">
            <thead>
              <tr>
                <th scope="col">Item Name</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
                <th scope="col" class="text-right">Total Price</th>
              </tr>
            </thead>
            <tbody>
              @php 
                $total_price=0;
              @endphp
              @foreach($cartitems['product'] as $item)
              <tr>
                <td>
                  <div class="cartProductInfo d-flex">
                    <div class="cartThumb"><img src="{{asset($item['image'])}}" alt=""></div>
                    <p>{{$item['title']}} <br>
                      <span class="removeButton"><a href="#" onclick="removeProduct({{$item['id']}})">Remove</a></span></p>
                  </div>
                </td>
                <td>{{$item['qty']}}</td>
                <td>{{$item['price']}}</td>
                <td class="price">${{$item['qty']*$item['price']}}</td>
              </tr>
              @php
                $total_price +=$item['price']*$item['qty'];
              @endphp
              @endforeach
            </tbody>
          </table>
          @if(isset($total_price) && $total_price>0)
          <div class="cartFinalInfo text-right">
            <div class="priceHeading">Subtotal: ${{$total_price}}<sup>00</sup></div>
            <!-- <p><em>Taxes and shipping calculated at checkout<em></p> -->
            <div><button type="submit" class="ml-1 mr-1 customBtn01">Pay Now</button></div>
          </div>
          @endif
          @endif
        </div>
      </div>
       </form>
    </div>
  </div>
</section>
<script src="{{ asset('public/frontend/js/jquery.min.js') }}"></script> 
<script>
  $('.plus').on('click',function(e){

    var val = parseInt($(this).prev('input').val());
    var qtry=val+1;
    var productid=$(this).data('item');
    console.log(productid+'--'+qtry);
    updateCartValue(productid,qtry)

    });
    $('.minus').on('click',function(e){

    var val = parseInt($(this).next('input').val());
    if(val > 0){
        var qtry=val-1;
        var productid=$(this).data('item');
        console.log(productid+'--'+qtry);
        updateCartValue(productid,qtry)
    } });


function updateCartValue(product_id,qty)
{
  var csrf_token = $('meta[name=csrf-token]').attr('content');
    $.ajax({
      url: 'update-cart',
      type: 'POST',
      data: {_token: csrf_token, productid:product_id,quantity:qty},
      success: function (data) { 
        location.reload(1);
      }
    });
}

</script>
@endsection('content')