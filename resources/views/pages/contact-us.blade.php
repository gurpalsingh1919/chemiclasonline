@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> Contact Us</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    @if(session('error')) 
            <div class="col-md-12 error alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Error : </strong>   {{ session('error') }}
            </div>
          @endif
          @if(session('success')) 
            <div class="col-md-12 error alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! session('success') !!}
            </div>
          @endif
    <div class="row">
      <div class="col-md-12">
        <h1>CONTACT US</h1>

        <div>
          <h2>HAVE A QUESTION?</h2>
          <h3>Speak with Customer Service:</h3>
          <p>1-800-243-5360,2</p>
          <p>customer.service@greenfield.com</p>
          <h3>Speak to A Live Sales Person (M-F 8AM-5PM EST):</h3>
          <p>1-800-243-5360,1</p>
          <h3>Email Marketing:</h3>
          <p>marketing@greenfield.com</p>
          <h3>Technical Questions &amp; Documentation Requests:</h3>
          <p>techsupport@greenfield.com</p>
          <h3>Speak with Accounting:</h3>
          <p>1-800-243-5360,3</p>
          <h3><a class="" href="#" target="_blank">Frequently Asked Questions</a></h3>
          <br>
          <h2>REQUEST A QUOTE / SUPPORT PRICING</h2>
          <form method="post" action="{{route('pages.contact_post')}}">
            @csrf
          
            <label for="companyname">Company Name</label> 
            <input type="text" id="companyname" name="company_name"> 

            <label for="fname">First Name</label> 
            <input type="text" id="fname" name="first_name"> 

            <label for="lname">Last Name</label> 
            <input type="text" id="lname" name="last_name"> 

            <label for="emailaddr">E-mail Address</label> 
            <input type="email" id="emailaddr" name="email_address"> 
            @error('email_address')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              @enderror

            <label for="phonenumber">Phone Number</label> 
            <input type="text" id="phonenumber" name="phone_number"> 
            @error('phone_number')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              @enderror
            <label for="comments">Comments</label> 
            <input type="text" id="comments" name="comments"> 

            <input type="submit" value="SUBMIT" class="submitBtn">
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection('content')