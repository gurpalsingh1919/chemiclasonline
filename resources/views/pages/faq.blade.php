@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> FAQ'S</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>FAQ'S</h1>
        <div>
          <h3>1) HOW CAN I SET UP AN ACCOUNT?</h3>
          <p>If you are an existing customer, our Customer Service team can provide you with your User ID and Password. If you are a new customer, you can apply to set up an account with us by <a class="" href="#" target="_self">clicking here</a>. Our Customer Service team can be reached at 1-800-243-5360 or <a class="" href="mailto:%20customer.service@pharmachemonline.com" target="_blank">customer.service@pharmachemonline.com</a>.</p>
          <h3>2) WHAT FORMS OF PAYMENT DO YOU ACCEPT?</h3>
          <p>We accept credit card payments from Visa, Mastercard, and American Express. In addition, if you have existing payment terms with Greenfield Global you will have the option to "Pay on Account" and will be sent an invoice for your order in accordance with agreed upon terms.</p>
          <h3>3) WHO SHOULD I CONTACT IF I HAVE PROBLEMS WITH THE ONLINE STOREFRONT?</h3>
          <p>Please contact our Customer Service team at 1-800-243-5360,2.</p>
          <h3>4) WHERE CAN I FIND YOUR TERMS AND CONDITIONS?</h3>
          <p>Click <a class="" href="{{route('pages.terms_conditions')}}" rel="" target="_blank">HERE</a> for our terms and conditions.</p>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection('content')