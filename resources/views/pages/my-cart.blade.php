@extends('layouts.frontend-app')

@section('content')
<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="success-message"></div>
      <div class="col-md-12">
        <h2>Shopping Cart</h2>
        <div class="cartBlock">
          @if(isset($cartitems['product']) && count($cartitems['product'])>0)
          <table class="table table-striped cart w-100">
            <thead>
              <tr>
                <th scope="col">Item Name</th>
                <th scope="col">Quantity</th>
                <th scope="col">Price</th>
                <th scope="col" class="text-right">Total Price</th>
              </tr>
            </thead>
            <tbody>
              @php 
                $total_price=0;
              @endphp
              @foreach($cartitems['product'] as $item)
              <tr>
                <td><div class="cartProductInfo d-flex"><div class="cartThumb">
                  <img src="{{asset($item['image'])}}" alt=""></div><p>{{$item['title']}}<br><span class="removeButton">
                    <a href="#" onclick="removeProduct({{$item['id']}})">Remove</a></span></p></div></td>
                <td>
                  <div class="quantity buttons_added">
                    <input type="button" value="-" class="minus" data-item="{{$item['id']}}">
                    <input type="number" step="1" min="1" max="" name="quantity" value="{{$item['qty']}}" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
                    <input type="button" value="+" class="plus" data-item="{{$item['id']}}">
                  </div>
                </td>
                <td class="price">${{$item['price']}}</td>
                <td class="price">${{$item['price']*$item['qty']}}</td>
              </tr>
              @php
                $total_price +=$item['price']*$item['qty'];
              @endphp
              @endforeach
             
            </tbody>
          </table>
          @if(isset($total_price) && $total_price>0)
          <div class="cartFinalInfo text-right">

            <div class="priceHeading">Subtotal: ${{$total_price}}<sup>00</sup></div>
            <p><em>Taxes and shipping calculated at checkout<em></p>
            <div>
              <a href="{{route('collections')}}" class="ml-1 mr-1 customBtn01 transparentBtn">Continue Shopping</a> 
              <a href="{{route('checkout')}}" class="ml-1 mr-1 customBtn01"><i class="fa fa-shopping-cart"></i> Checkout</a>
            </div>
          </div>
          @endif
          @else

          <div class="col-md-12">
            <div class="mt-5 mb-5 text-center">
              <div class="emptyCartIcon mt-2 mb-3"><i class="fa fa-shopping-basket" aria-hidden="true"></i></div>
              <h1 class="display-4 mb-3">Your Cart is Empty</h1>
              <p class="lead  mb-4"><strong>Add something to make me happy :)</p>
              <hr>
              <p class="lead">
                <a href="{{route('home')}}" class="ml-1 mr-1 customBtn01 transparentBtn">Back to Homepage</a> 
                <a href="{{route('collections')}}" class="ml-1 mr-1 customBtn01">Continue Shopping</a>
              </p>
            </div>
          </div>
          @endif
        </div>
      </div>

    </div>
  </div>
</section>
<script src="{{ asset('public/frontend/js/jquery.min.js') }}"></script> 
<script>
  $('.plus').on('click',function(e){

    var val = parseInt($(this).prev('input').val());
    var qtry=val+1;
    var productid=$(this).data('item');
    console.log(productid+'--'+qtry);
    updateCartValue(productid,qtry)

    });
    $('.minus').on('click',function(e){

    var val = parseInt($(this).next('input').val());
    if(val > 0){
        var qtry=val-1;
        var productid=$(this).data('item');
        console.log(productid+'--'+qtry);
        updateCartValue(productid,qtry)
    } });


function updateCartValue(product_id,qty)
{
  var csrf_token = $('meta[name=csrf-token]').attr('content');
    $.ajax({
      url: 'update-cart',
      type: 'POST',
      data: {_token: csrf_token, productid:product_id,quantity:qty},
      success: function (data) { 
        location.reload(1);
      }
    });
}

</script>
@endsection('content')