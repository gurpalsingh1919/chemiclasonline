@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> NEWS</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection newsSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>NEWS</h1>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">CANADA’S FIRST-EVER FEASIBILITY STUDY ON THE CASE FOR NUCLEAR HYDROGEN PRODUCTION NOW UNDERWAY</a></h2>
        <p>NII to explore potential for hydrogen demonstration plant in Bruce County (Port Elgin, Ontario) The Nuclear Innovation Institute (NII) has launched a new study into the role of nuclear power in supporting a growing hydrogen economy. The study will be the first of its kind in Canada to evaluate the technical viability and business case […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> 
        <img src="{{asset('public/frontend/images')}}/Hydrogen-feasibility-study-Greenfield-quote.png" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Greenfield_CAM-NewsPost.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">SPECIALITY CHEMICAL &amp; INGREDIENTS CUSTOMER APPRECIATION MONTH – JUNE 2021</a></h2>
        <p>In 1991, Greenfield Global, then Sunroot, acquired a small business known as Commercial Alcohols Ltd. This venture started Greenfield down a path of supplying high-purity specialty chemicals and ingredients, under the coveted Pharmco brand, to our commercial, life science, herbal extraction, food, flavor and fragrance industry partners the world over. What better way to celebrate […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL SIGNS AGREEMENT WITH CFS FOR ITS WINNEBAGO, MINNESOTA PLANT</a></h2>
        <p>Leading Ethanol Producer Partners with CFS for Corn Supply and DDGS Offtake WINNEBAGO, MN, June 2, 2021— Greenfield Global Inc., a leading producer of renewable energy solutions and a global leader in the production of high-purity specialty alcohols and solvents, announced today that it has reached an agreement with Central Farm Service (CFS) for the supply […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/top-3.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Howard-Field-small.png" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">HOWARD FIELD RECEIVES THE 2021 CLEAN50 &amp; CLEAN16 SUSTAINABILITY AWARD</a></h2>
        <p>Delta Management Group and the Clean50 organization announced today Canada’s prestigious Clean50 Awards, which recognize 50 individuals who have done the most to advance the cause of sustainability and clean capitalism in Canada over the past two years. Greenfield is thrilled to share that, in addition to being chosen as part of the 2021 Clean50 cohort, […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL IN BROOKFIELD, CONNECTICUT, RECEIVES EXCIPACT GMP CERTIFICATION</a></h2>
        <p>First Ethanol Supplier to be Awarded EXCiPACT GMP Certification in the United States BROOKFIELD, Conn., December 8, 2020 /[CNW]/ — Greenfield Global Inc., one of Canada’s largest ethanol producers and a global leader in the production high-purity specialty alcohols and solvents, announced today that it has been awarded Excipient Quality System (EXCiPACT) Good Manufacturing Practice (GMP) certification […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Greenfield_EXCiPACT-PressRelease.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/highway2-002-cropped-1995x2048.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL APPLAUDS ONTARIO GOVERNMENT’S MOVE TO CLEANER AND GREENER GASOLINE</a></h2>
        <p>Ontario’s boost to ethanol blending will mean lower greenhouse gas emissions and more affordable fuel for consumers in the province. Toronto, Ontario, November 26, 2020 /[CNW]/ — Greenfield Global Inc., Ontario’s first and one of Canada’s largest ethanol producers, today applauded the Ontario government’s announcement of increased requirements for renewable content in regular-grade gasoline from […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL TO ADD 48 MILLION GALLONS TO BIOFUELSPRODUCTION WITH ACQUISITION OF MINNESOTA FUEL ETHANOL PLANT</a></h2>
        <p>The former Corn Plus facility in Winnebago, Minnesota, is Greenfield’s first fuel ethanol asset in the United States Toronto, Ontario, October 21, 2020 /[CNW]/ — Greenfield Global Inc., Canada’s largest fuel ethanol producer, announced today that it will acquire the 182 million litre (48 million gallon) per-year Corn Plus ethanol facility in Winnebago, Minnesota. The […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/corn-plus.png" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/JT-Expansion-photo.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL ANNOUNCES A 100 MILLION LITRE HIGH PURITY ALCOHOL EXPANSION IN ONTARIO</a></h2>
        <p>Greenfield to produce 100 Million Litres per year of US Pharmacopeia Grade (USP) alcohol at its facility in Johnstown, Ontario. Johnstown, Ontario, October 16, 2020 /[CNW]/ — Greenfield Global Inc., Canada’s largest producer of fuel ethanol, and a global leader in the production high-purity specialty alcohols &amp; solvents, announced today that it will begin producing […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL PRESENTS ITS NEW EUROPEAN MANUFACTURING HEADQUARTERS IN IRELAND</a></h2>
        <p>Portlaoise 28th July 2020 Greenfield Global Inc., a global leader in the production of ethanol, high-purity speciality alcohols, and solvents, will soon commence the commissioning phase of their new EU Manufacturing Headquarters in Portlaoise, Ireland. The new 3,800 sq. metre facility, the company’s first outside North America, will produce Pharmco branded products serving Life Science […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Ireland_Linkedin.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Longhorn-vaccines.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">LONGHORN VACCINES AND DIAGNOSTICS LLC, AND GREENFIELD GLOBAL INC. PARTNER TO ACCELERATE COVID-19 TESTING AND DETECTION</a></h2>
        <p>Longhorn has selected Greenfield Global’s Pharmco brand high purity reagent alcohol to quickly distribute its PrimeStore MTM® worldwide. BROOKFIELD, Conn., June 15, 2020 /PRNewswire/ — Longhorn Vaccines &amp; Diagnostics, an innovative molecular tool, assay and vaccine development company, and Pharmco, Greenfield Global’s popular brand of specialty alcohols and high-purity solvents, have combined to rapidly accelerate coronavirus testing and […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL DONATING $400,000 TO HELP LOCAL COMMUNITIES FACING COVID-19 HARDSHIPS</a></h2>
        <p>Donations are being made through the company’s newly established Greenfield Cares fund and directed to local charities in communities where Greenfield Global employees live and work. TORONTO, May 22, 2020 – Greenfield Global Inc., a global leader in the production of ethanol, high-purity specialty alcohols, and solvents, today announced the creation of the Greenfield Cares Fund, dedicated […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Greenfield_CaresFund_Linkedin.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Covid-19_PressRelease-002.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL DEVELOPING INNOVATIVE SOLUTIONS TO FIGHT THE SPREAD OF COVID-19</a></h2>
        <p>Greenfield’s distilleries and manufacturing facilities are developing new formulations to add additional supply of critically needed hand sanitizers and disinfectants TORONTO, March 24, 2020 – Greenfield Global Inc., a global leader in ethanol, high-purity specialty alcohols and solvents production, has been producing and shipping vital alcohols and solvents at a record pace to combat the worldwide […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL INC. AND HY2GEN CANADA ANNOUNCE A JOINT VENTURE TO PRODUCE GREEN HYDROGEN IN QUÉBEC</a></h2>
        <p>The partnership will use the strengths of both companies to produce and deploy 100 percent carbon-free green hydrogen in the Greater Montréal area as well as expansion to fast-growing international markets. Québec City, Québec, Canada, November 20, 2019 – Greenfield Global Inc., Canada’s largest ethanol producer, and Hy2gen Canada, the first mover for commercial production […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/GettyImages-1063648066-800x533.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/CPhI_PressRelease.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO AT CPHI WORLDWIDE SHOWCASING NEW EU MANUFACTURING FACILITY AND BIOPROCESS UPGRADES</a></h2>
        <p>The leading international manufacturer of specialty alcohols and bio-based chemicals reaches global scale for life science customers shipping from the United States and Europe. BROOKFIELD, Connecticut, November 4, 2019 – Pharmco, a brand of Greenfield Global, a leading international manufacturer of specialty alcohols and bio-based chemicals, will be an exhibitor at the CPhI Worldwide conference, November […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL TAKES NEXT STEPS TO EXPAND PRODUCTION AT ITS VARENNES BIOREFINERY FOLLOWING DRAFT REGULATION ON THE MINIMUM RENEWABLE FUEL VOLUMES IN QUEBEC</a></h2>
        <p>Province’s largest ethanol producer says the policy could result in a major expansion of the output of its production in Quebec. VARENNES, QC, 11 October 2019— Greenfield Global Inc., Canada’s largest ethanol producer and a world leader in high-purity specialty alcohols, welcomed the Government of Quebec’s pre-publication of draft regulations on the minimum volume of renewable […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Varennes-copy.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/BioprocessingExpansion_PressRelease.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO ANNOUNCES CONNECTICUT BIOPROCESSING WING EXPANSION</a></h2>
        <p>Expanded capabilities include Aseptic Filling to support Single Use Biologic manufacturing and temperature-controlled storage for stability programs. Brookfield, Connecticut September 10, 2019 — Pharmco, a leading international producer of specialty alcohols and high purity solvents, announced an expansion to its packaging facility in Brookfield, Connecticut, through the creation of a bioprocessing wing. The new capabilities include aseptic filling […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO OPENS NEW WAREHOUSE IN WEST HAVEN, CONNECTICUT</a></h2>
        <p>Distribution center adds 60,000 square feet of capacity; expanding the company’s rapidly growing footprint and creating faster delivery capacity to five North Eastern States. West Haven, Connecticut August 28, 2019 — Pharmco, a leading international producer of specialty alcohols and high purity solvents, announced today that it will expand its distribution capabilities by adding storage […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/NewHavenWarehouse_PressRelease.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/FloridaWarehouse_PressRelease-1.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO ANNOUNCES NEW WAREHOUSE IN OCALA, FLORIDA FOR ITS INDUSTRY-LEADING SPECIALTY ALCOHOLS AND HIGH PURITY SOLVENTS</a></h2>
        <p>New 265,000 square foot warehouse in prime location expands the brand’s distribution and better supports specialty and high purity solvents to customers from coast-to-coast. Tampa Bay, Florida July 10, 2019 — Pharmco, a leading international producer of specialty alcohols and high purity solvents, announced today that it will expand its distribution capabilities by opening a 265,000 square foot […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO, LEADING INTERNATIONAL MANUFACTURER OF SPECIALTY ALCOHOLS AND BIO-BASED CHEMICALS, SHOWCASING ITS INTERNATIONAL FLAVORS AND FRAGRANCE PORTFOLIO AT IFT19.</a></h2>
        <p>NEW ORLEANS, LA, 04 June 2019— Pharmco, a brand of Greenfield Global, a leading international manufacturer of specialty alcohols and bio-based chemicals, has been exhibiting at the Institute of Food Technologists (IFT) Annual Event and Food Expo show this week at the Ernest N. Morial Convention Center in New Orleans, LA. IFT is one of […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/IFT_Linkedin-2-00000002.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/NonOrganicCane_PressRelease.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO COMPLETES COMMISSIONING AND BEGINS DELIVERIES FROM THE LARGEST NON-GMO CANE ALCOHOL TERMINAL IN NORTH AMERICA</a></h2>
        <p>The dedicated capacity in Carteret, New Jersey, adds 800,000 gallons of storage and is the only one of its kind on the East Coast. Brookfield, CT, XX May 2019— Pharmco, a brand of Greenfield Global, is proud to announce that its new dedicated storage tanks at Kinder Morgan Terminal in Carteret, New Jersey, are fully […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL INC. TO ESTABLISH EUROPEAN MANUFACTURING FACILITY IN IRELAND.</a></h2>
        <p>The company is expanding operations and building local capacity to better serve life sciences customers throughout Europe and the region. Brookfield, CT. April 16, 2019 – Greenfield Global Inc., Canada’s largest producer of alcohol and fuel ethanol, and one of the largest alcohols and solvents companies in North America, has announced plans to establish a new […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Greenfield_PressRelease_FieldQuote-1-2.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Pharmco_StainlessSteel_Linkedin_1200X675-00000003.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO ADDS STAINLESS STEEL RETURNABLE FLEETS FOR LIFE SCIENCE CUSTOMERS IN NORTH AMERICA</a></h2>
        <p>New Packaging Option Expands High Purity Solvent &amp; Ethanol Portfolio and Reduces Customer Waste. Brookfield, CT, 2nd April 2019— Pharmco, a brand of Greenfield Global, today announced it has expanded its portfolio options to include Stainless Steel Pressurized Vessels at the company’s Brookfield, CT, USA facility. This new portfolio option further strengthens Pharmco’s unique production […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO ESTABLISHES LARGEST STORAGE CAPACITY AND SUPPLY CHAIN CAPABILITIES FOR NON GMO CANE ALCOHOL IN NORTH AMERICA</a></h2>
        <p>CARTERET, N.J., Feb. 5, 2019 /PRNewswire/ — Pharmco, a brand of Greenfield Global, today announced it is expanding its storage capacity and supply chain capabilities at the Kinder Morgan Terminal in Carteret, New Jersey. The newly acquired storage tanks provide 800,000 gallons of dedicated storage that will serve customers supplying Non-GMO sugar cane alcohol. Pharmco’s East Coast terminal is […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/OrganicCane_Linkedin.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/RotEvaporation.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO’S KOSHER CERTIFIED HERBAL EXTRACTION SOLVENT IS NOW AVAILABLE TO CUSTOMERS ANYWHERE IN NORTH AMERICA</a></h2>
        <p>SHELBYVILLE, Ky., Feb. 19, 2019 /PRNewswire/ — Pharmco, by Greenfield Global, today announced it is expanding the availability of its popular Kosher Certified herbal extraction solvent, sold as CDA 12A-1, to all regions in Canada and the United States. The product is a 200-proof grain denatured alcohol made with 4.76 percent high purity n-heptane that is exempt from the federal excise […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">PHARMCO EXPANDS US MANUFACTURING FOR HERBAL EXTRACTION SOLVENTS</a></h2>
        <p>https://www.prnewswire.com/news-releases/pharmco-expands-us-manufacturing-for-herbal-extraction-solvents-300767427.html</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/KY_March-2019_13.jpg" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Ontario-support-of-ethanol-news.jpg" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL WELCOMES ONTARIO GOVERNMENT’S SUPPORT OF ETHANOL</a></h2>
        <p>Greener Gasoline regulation means lower greenhouse gases and more affordable fuel for consumers in the province. Toronto, ON 13 April 2017 — Greenfield Global Inc., Ontario’s first and Canada’s largest ethanol producer, applauds the Ontario government’s decision to increase ethanol blending in the province. The changes to the existing regulation will require gasoline suppliers to […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">JEAN-FRANCOIS (“JF”) HUC JOINS GREENFIELD GLOBAL AS VICE PRESIDENT OF INNOVATION &amp; BUSINESS DEVELOPMENT</a></h2>
        <p>Adding to a group of key executives that have joined Greenfield in 2017 TORONTO, Sept. 13, 2017 /CNW/ – Greenfield Global, a leading global producer of high-purity alcohols, biofuels and biochemicals, today announced that JF Huc has joined the company in a newly created position as Vice President of Innovation &amp; Business Development. JF joins three other key […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/JF-news.png" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Investing-in-sustainable-greenhouse-news.png" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">INVESTING IN SUSTAINABLE GREENHOUSE GROWING SYSTEMS</a></h2>
        <p>CHATHAM, ON, Sept. 11, 2017 /CNW/ – Canada’s agriculture and agri-food industry is an important driver of economic growth in Canada. The Government of Canada continues to support the sector as it reduces emissions and adopts sustainable practices that lead to more efficient use of water and energy. Member of Parliament Peter Fragiskatos (London North Centre), on behalf of Agriculture Minister Lawrence MacAulay, […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD GLOBAL EVALUATING MAJOR EXPANSION OF BIOFUELS PRODUCTION IN VARENNES, QUEBEC</a></h2>

        <p>VARENNES, QC, Aug. 15, 2017 /CNW/ – Greenfield Global Inc., (formerly GreenField Specialty Alcohols Inc.) Canada’s largest ethanol producer and a world leader in high-purity specialty alcohols, announced today that it has commenced a feasibility study to significantly expand operations for sustainable biofuel production at its biorefinery in Varennes, Quebec. The first ethanol plant built in Quebec, the Varennes distillery is a model […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Evaluating-mejor-expansion-news.png" alt="" class="imgResponsive" > </div>
    </div>
    <div class="row newsList">
      <div class="col-md-6"> <img src="{{ asset('public/frontend/images')}}/Changing-name-news.png" alt="" class="imgResponsive" > </div>
      <div class="col-md-6">
        <h2><a href="#" target="_self">GREENFIELD SPECIALTY ALCOHOLS INC. ANNOUNCES NAME CHANGE TO GREENFIELD GLOBAL INC.</a></h2>
        <p>New Name Reflects Strength of Product Lines, Global Reach, and Scale TORONTO, ON, 25 May 2017— GreenField Specialty Alcohols Inc., Canada’s largest ethanol producer and a world leader in high-purity specialty alcohols, announced today it has changed its name to Greenfield Global. The name change reflects the company’s broader product categories and its expertise in driving […]</p>
        <p><a href="#" target="_self">Read Full Article</a></p>
      </div>
    </div>
  </div>
</section>
@endsection('content')