@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> 
          <span>></span> {{(isset($pagedetail->title))?$pagedetail->title:''}}</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
     	{!! $pagedetail->content!!}
    </div>
  </div>
</section>
<style type="text/css">
  .flex_container_pd {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
.rte {
    margin-bottom: 15px;
}
.flex_container_pd > .flex_item_left_1_pd {
    width: 33%;
}
.flex_container_pd > .flex_item_right_2_pd {
    width: 67%;
    padding-left: 2%;
}
.rte table {
    table-layout: fixed;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    min-width: 50%;
}
th, td {
    text-align: left;
    padding: 15px;
    border: 1px solid #dedede;
}
.flex_container_pd_supporting {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
.flex_container_pd_supporting > .flex_item_pd_supporting {
    text-align: center;
    width: 20%;
}

a {
    color: #9c756f;
    text-decoration: none;
    background: transparent;
}
img.auto, .grid-item img, .grid-item iframe {
    max-width: 100%;
}
</style>
@endsection('content')