@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> 
          <span>></span> {{(isset($pagedetail->title))?$pagedetail->title:''}}</div>
      </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="productDocumentPages">
	  <div class="container">
		<div class="row">
			{!! $pagedetail->content!!}
		</div>
	  </div>
  </div>
</section>

@endsection('content')