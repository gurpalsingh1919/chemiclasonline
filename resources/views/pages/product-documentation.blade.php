@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="#">Home</a> <span>></span> PRODUCT DOCUMENTATION</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
           <h1>PRODUCT DOCUMENTATION</h1>
         </div>
         <h2>PRODUCT LIST</h2>

         <p class="col-md-12">Select any product to access more information including specifications, safety data sheets and more.</p>
         @foreach($results as $key=>$result)
         <div class="product-list-{{strtolower($key)}}">
            <h3>{{$key}}</h3>
            <div class="product-list-flex-container col-md-12">
               @if(isset($result) && count($result)>0)
                  @foreach($result as $page)
                     <h4 class="col-md-6"><a href="{{route('pages_details',$page->slug)}}"  rel="nofollow" target="_self">{{$page->title}}</a></h4>
                  @endforeach
               @endif
            </div>
         </div>
         @endforeach
      </div>
   </div>
</section>
@endsection('content')