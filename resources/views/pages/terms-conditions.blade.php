@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> Terms and Conditions</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Terms and Conditions</h1>

        <div>
          <object data="{{ asset('public/frontend/pdf/Terms-and-Conditions-07Dec2020.pdf') }}" type="application/pdf" width="1100" height="1200">
            <a href="{{ asset('public/frontend/pdf/Terms-and-Conditions-07Dec2020.pdf') }}">terms and conditions.pdf</a>
          </object>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection('content')