@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> Change Password</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      @include('user.left-sidebar')
    <div class="col-md-8 col-lg-9">
      <h2>Change Password</h2>
      <div class="cartBlock">
        <form method="POST" action="{{ route('user.update_password') }}">
          @csrf
          @if(session('error')) 
            <div class="col-md-12 error alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Error : </strong>   {{ session('error') }}
            </div>
          @endif
          @if(session('success')) 
            <div class="col-md-12 error alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! session('success') !!}
            </div>
          @endif
          <div class="row">
            <div class="col-md-6">
              <label for="current_password">Current Password</label> 
              <input id="current_password" type="password" class="@error('current_password') is-invalid @enderror" name="current_password" value="{{ old('current_password')}}" autocomplete="current_password" autofocus>
              @error('current_password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="password">New Password</label> 
              <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" value="{{ old('password') }}"  autocomplete="password">
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="password_confirmation">Confirm New Password</label> 
              <input id="password_confirmation" type="text" class="@error('password_confirmation') is-invalid @enderror" name="password_confirmation" autocomplete="password_confirmation" >
              @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            
          </div>
          <input type="submit" value="Update" class="submitBtn">
        </form>
      </div>
    </div>
  </div>
</div>
</section>
@endsection('content')