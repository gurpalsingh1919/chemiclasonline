<div class="col-md-4 col-lg-3">
 <!--  <div class="userImage"><img src="images/Howard-Field-small.png" class="imgResponsive" > </div> -->
  <h3 class="userName">{{ Auth::user()->name }}</h3>
  <hr class="d-block">
  <ul class="userSettingList">
    <li class="active"><a href="{{route('user.my_account')}}"><i class="fa fa-user-o" aria-hidden="true"></i> My Account</a></li>
    <!-- <li><a href="#"><i class="fa fa-bell-o" aria-hidden="true"></i> Notifications</a></li> -->
    <li><a href="{{route('user.change_password')}}"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a></li>
    <!-- <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i> My Wishlist</a></li> -->
    <li><a href="{{route('user.transaction_history')}}"><i class="fa fa-list-ul" aria-hidden="true"></i> My Order</a></li>
    <li>
      <a href="{{ route('logout') }}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i class="fa fa-power-off" aria-hidden="true"></i> {{ __('Logout') }}
          </a>
      
    </li>
  </ul>
</div>