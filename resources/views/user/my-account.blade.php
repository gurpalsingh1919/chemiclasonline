@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> My Account</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      @include('user.left-sidebar')
    <div class="col-md-8 col-lg-9">
      <h2>My Account</h2>
      <div class="cartBlock">
        <form method="POST" action="{{ route('user.update_account') }}">
          @csrf
          @if(session('error')) 
            <div class="col-md-12 error alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Error : </strong>   {{ session('error') }}
            </div>
          @endif
          @if(session('success')) 
            <div class="col-md-12 error alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! session('success') !!}
            </div>
          @endif
          <div class="row">
            <div class="col-md-6">
              <label for="first_name">First Name</label> 
              <input id="first_name" type="text" class="@error('first_name') is-invalid @enderror" name="first_name" value="{{ Auth::user()->first_name }}" required autocomplete="name" autofocus>
              @error('first_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="last_name">Last Name</label> 
              <input id="last_name" type="text" class="@error('last_name') is-invalid @enderror" name="last_name" value="{{ Auth::user()->last_name }}" required autocomplete="last_name" autofocus>
              @error('last_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="Email">Email</label> 
              <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email" disabled>
              @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="fname">Company</label> 
              <input id="company" type="text" class="@error('company') is-invalid @enderror" name="company" value="{{(Auth::user()->company !='')?Auth::user()->company:old('company')}}" required>
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
             <div class="col-md-12">
              <label for="fname">Address1</label> 
              <input id="address1" type="text" class="@error('address1') is-invalid @enderror" name="address1" value="{{(Auth::user()->address1 !='')?Auth::user()->address1:old('address1')}}" required>
              @error('address1')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
             <div class="col-md-12">
              <label for="address2">Address2</label> 
              <input id="address2" type="text" class="@error('address2') is-invalid @enderror" name="address2" value="{{(Auth::user()->address2 !='')?Auth::user()->address2:old('address2')}}" required>
              @error('address2')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="city">City</label> 
              <input id="city" type="text" class="@error('city') is-invalid @enderror" name="city" value="{{(Auth::user()->city !='')?Auth::user()->city:old('city')}}" required>
              @error('city')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="country">Country</label> 
              <select name="country" id="country" class="@error('country') is-invalid @enderror">
                @foreach($countries as $country)
                  <option value="{{$country->id}}" {{(Auth::user()->country==$country->id || old('county')==$country->id)?"selected":''}}>{{$country->name}}</option>
                @endforeach
              </select>
              @error('country')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="zip_code">Postal/Zip code</label> 
              <input id="zip_code" type="text" class="@error('zip_code') is-invalid @enderror" name="zip_code" value="{{(Auth::user()->zip_code !='')?Auth::user()->zip_code:old('zip_code')}}" required>
              @error('zip_code')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="col-md-6">
              <label for="phone">Phone</label> 
              <input id="phone" type="text" class="@error('phone') is-invalid @enderror" name="phone" placeholder="555-555-1234" value="{{(Auth::user()->phone !='')?Auth::user()->phone:old('phone')}}" required>
              @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>
          <input type="submit" value="Update" class="submitBtn">
        </form>
      </div>
    </div>
  </div>
</div>
</section>
@endsection('content')