@extends('layouts.frontend-app')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <div class="breadcrumb"><a href="{{route('home')}}">Home</a> <span>></span> Login</div>
    </div>
    </div>
  </div>
</section>
<section class="contentSection">
  <div class="container">
    <div class="row">
      @include('user.left-sidebar')
    <div class="col-md-8 col-lg-9">
      <h2>Order History</h2>
      <div class="cartBlock">
        <table class="table table-striped cart w-100">
          <thead>
            <tr>
              <th scope="col">Order ID</th>
              <th scope="col">Order Date</th>
              <th scope="col">Order Number</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                <td>1</td>
                <td>16-11-2021</td>
                <td>USA1324789012</td>
                <td class="text-success"><strong>Deliverd</strong></td>
              </tr>
              <tr>
                <td>2</td>
                <td>16-11-2021</td>
                <td>USA1324789012</td>
                <td class="text-danger"><strong>Pending</strong></td>
              </tr>
              <tr>
                <td>3</td>
                <td>16-11-2021</td>
                <td>USA1324789012</td>
                <td><strong>In Process</strong></td>
              </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection('content')