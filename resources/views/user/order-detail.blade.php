@extends('layouts.frontend-app')

@section('content')

<section class="contentSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	     <!--  <h2>Invoice</h2> -->
        <div class="mt-5 text-center">
          <h1 class="display-4 mb-3">Invoice</h1>
         
        </div>
        <div class="row">
          <div class="col-md-6 text-left">
           <p class="lead">
            <strong>Payment ID:- </strong> {{$transactions->payment_id}}</p>
            <p class="lead">
            <strong>Invoice ID:- </strong> {{$transactions->id}}</p>
            <p class="lead">
            <strong>Date:- </strong> {{date_format($transactions->created_at, 'F j, Y')}}</p>
          </div>
          <div class="col-md-6 mt-5 mb-5 text-right">
            <p class="lead">
              <strong>Status:- </strong>
              @if($transactions->status=='1')
                <badge class="btn-sm btn-success">Success</badge>
              @elseif($transactions->status=='2')
                <badge class="btn-sm btn-info">Fail</badge>
              @elseif($transactions->status=='3')
                <badge class="btn-sm btn-danger">Cancel</badge>
              @else
                <badge class="btn-sm btn-secondary">Pending</badge>
              @endif
            </p>      
            <p class="lead">
            <strong>Name:- </strong> {{$transactions->user->first_name .' '.$transactions->user->last_name}}</p>
          </div>
        </div>
        <hr>
		    <div class="cartBlock">
          <table class="table table-striped cart w-100">
            <thead>
              <tr>
				        <th scope="col">#</th>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Sub Total</th>
              </tr>
            </thead>
            <tbody>
              @if(isset($transactions->orderdetail) && count($transactions->orderdetail)>0)
              @foreach($transactions->orderdetail as $key=>$order)
              <tr>
				        <td>{{$loop->iteration}}</td>
                <td>{{ $transactions->product[$key]['title']}}</td>
                <td>${{$order->price}}</td>
                <td>{{$order->qty}}</td>
                <td class="text-success"><strong>${{number_format($order->price * $order->qty, 2)}}</strong></td>

              </tr>
              @endforeach
               <tr>
                 <td colspan="4" class="text-right"><b>Total</b></td>
                 <td class="text-success"><strong>${{$transactions->amount}}</strong></td>
               </tr>
              @endif
              
            </tbody>
          </table>

          <div class="mt-5 mb-5 text-center">
          <h1 class="display-4 mb-3">Thank You!</h1>
         
          <p class="mt-4">
            Having trouble? <a href="{{route('pages.contact_us')}}">Contact us</a>
          </p>
          <p class="lead">
            <a href="{{route('user.transaction_history')}}" class="ml-1 mr-1 customBtn01 transparentBtn">All Transactions</a> 
            <a href="{{route('collections')}}" class="ml-1 mr-1 customBtn01">Continue Shopping</a>
          </p>
        </div>

        </div>
		  </div>
    </div>
  </div>
</section>
@endsection('content')