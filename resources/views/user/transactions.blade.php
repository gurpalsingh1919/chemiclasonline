@extends('layouts.frontend-app')

@section('content')

<section class="contentSection">
  <section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb"><a href="{{route('user.my_account')}}">My Account</a> <span>></span>Order History</div>
      </div>
    </div>
  </div>
</section>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
	       <h2>Order History</h2>
		    <div class="cartBlock">
          <table class="table table-striped cart w-100">
            <thead>
              <tr>
				        <th scope="col">#</th>
                <th scope="col">Payment ID</th>
                <th scope="col">Payer ID</th>
                <th scope="col">Amount</th>
                <th scope="col">Order Date</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($ordertransactions as $order)
              <tr>
				        <td>{{$loop->iteration}}</td>
                <td>{{$order->payment_id}}</td>
                <td>{{$order->payer_id}}</td>
                <td>${{$order->amount}}</td>
                <td>{{ date_format($order->created_at, 'F j, Y') }}</td>
                <td class="text-success">
                  @if($order->status=='1')
                    <badge class="btn-sm btn-success">Success</badge>
                  @elseif($order->status=='2')
                    <badge class="btn-sm btn-info">Fail</badge>
                  @elseif($order->status=='3')
                    <badge class="btn-sm btn-danger">Cancel</badge>
                  @else
                    <badge class="btn-sm btn-secondary">Pending</badge>
                  @endif
                </td>
                <td><a href="{{route('user.transaction_detail',$order->id)}}" class="btn btn-info">View</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
		  </div>
    </div>
  </div>
</section>
@endsection('content')