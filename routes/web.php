<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.index');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/collections', 'HomeController@getCollections')->name('collections');
Route::get('/products', 'HomeController@getCollections');
Route::get('/search', 'HomeController@getSearchProducts')->name('product_search');
Route::get('/collections/{slug}', 'HomeController@getCollectionProducts')->name('collection_products');
Route::get('/products/{slug}', 'HomeController@getProductDetail')->name('product_details');
Route::get('/collections/pages/{slug}', 'HomeController@getPagesDetail')->name('pages_details');
Route::post('/products/add-to-cart', 'HomeController@addToCart')->name('add_to_cart');
Route::get('cart', 'HomeController@myShoppingCart')->name('my_cart');
Route::post('remove-from-cart', 'HomeController@removeFromCart');
Route::post('update-cart', 'HomeController@updateCart');
Route::get('checkout', 'HomeController@cartCheckout')->name('checkout');


Route::group(['as'=>'admin.','prefix' => 'admin','namespace'=>'Admin','middleware'=>['auth','admin']], function () {
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
		Route::get('products/add', 'DashboardController@addProduct')->name('add_product');
		Route::post('products/add', 'DashboardController@addProductPost')->name('add_product_post');
		Route::get('products', 'DashboardController@allProducts')->name('products');
		Route::get('products/{id}', 'DashboardController@getProductDetails')->name('product_details');
		Route::get('products/{id}/edit', 'DashboardController@getProductEdit')->name('product_edit');
		Route::post('products/{id}/edit', 'DashboardController@productUpdate')->name('product_update');
		Route::get('products/{id}/delete', 'DashboardController@deletProduct')->name('product_delete');
		Route::get('categories', 'DashboardController@allCategories')->name('categories');
		Route::get('category/{id}/products', 'DashboardController@getCategoryProducts')->name('category_products');

		Route::get('category/{id}/delete', 'DashboardController@deleteCategory')->name('category_delete');
		Route::get('category/add', 'DashboardController@addCategory')->name('add_category');
		Route::post('category/add', 'DashboardController@addCategoryPost')->name('add_category_post');

		Route::get('category/{id}/edit', 'DashboardController@getCategoryEdit')->name('category_edit');
		Route::post('category/{id}/edit', 'DashboardController@categoryUpdate')->name('category_update');

		Route::get('all-pages', 'DashboardController@productDocumentationPages')->name('all_pages');
		Route::get('add-new-page', 'DashboardController@addNewPage')->name('add_new_page');
		Route::post('add-new-page', 'DashboardController@addNewPagePost')->name('new_page_post');

		Route::get('page/{id}', 'DashboardController@getPageEdit')->name('page_edit');
		Route::post('page/{id}', 'DashboardController@pageUpdatePost')->name('page_update');
		Route::get('page/{id}/delete', 'DashboardController@deletePage')->name('page_delete');



});

Route::group(['as'=>'user.','prefix' => 'user','namespace'=>'User','middleware'=>['auth','user']], function () {
		Route::get('my-account', 'DashboardController@myAccount')->name('my_account');
		Route::post('my-account', 'DashboardController@updateUserPost')->name('update_account');
		Route::get('change-password', 'DashboardController@changePassword')->name('change_password');
		Route::post('change-password', 'DashboardController@changePasswordPost')->name('update_password');

		Route::post('pay-now', 'PayPalController@makepayment')->name('make_payment');
		Route::get('payment/success', 'PayPalController@success')->name('payment.success');
		Route::get('payment/cancel', 'PayPalController@cancel')->name('payment.cancel');

		Route::get('transactions', 'PayPalController@transactionHistory')->name('transaction_history');
		Route::get('transactions/{id}', 'PayPalController@transactionDetails')->name('transaction_detail');
});


Route::group(['as'=>'pages.','prefix' => 'pages'], function () {
		Route::get('news', 'PagesController@news')->name('news');
		Route::get('product-documentation', 'PagesController@productDocumentation')->name('product_documentation');
		Route::get('contact-us', 'PagesController@contactUs')->name('contact_us');
		Route::post('contact-us', 'PagesController@contactPost')->name('contact_post');
		Route::get('terms-conditions', 'PagesController@termsAndConditions')->name('terms_conditions');
		Route::get('faq', 'PagesController@faq')->name('faq');
		
		Route::get('about-us', 'PagesController@aboutUs')->name('about_us');
});